package com.isoft.belot.util;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Stefan Ivanov
 */
public class DummyUsersGenerator
{
    public static String[] names = {"Ivan", "Jack", "Xio ming", "Serj", "Anna", "Ema", "Milica"};
    public static String[] emails = {"Vankata@mail.bg", "JackNikolson@yahoo.com", "Sifuda91212@Yahoo.cn", "666Amarath666@gmail.com",  "Liglata@abv.bg", "EmZa96@yahoo.com", "milicaboneva95@mail.bg"};

    public static String getRandomName(){
        return names[ThreadLocalRandom.current().nextInt(0, names.length - 1)];
    }


}
