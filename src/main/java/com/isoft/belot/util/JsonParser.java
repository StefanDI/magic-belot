package com.isoft.belot.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author Aleksandar Todorov
 */
@Slf4j
@Component
public class JsonParser
{
    private ObjectMapper objectMapper;

    public JsonParser()
    {
        objectMapper = new ObjectMapper();
    }

    public String toJson(Object object)
    {
        try
        {
            return objectMapper.writer().writeValueAsString(object);
        } catch (JsonProcessingException e)
        {
            log.error("Jackson writer didn't work properly.", e);
        }
        return "";
    }

    public <T> T toObject(String json, Class<T> objectClass)
    {
        try
        {
            return objectMapper.readValue(json, objectClass);
        } catch (IOException e)
        {
            log.error("Error parsing json " + json, e);
            return null;
        }
    }
}
