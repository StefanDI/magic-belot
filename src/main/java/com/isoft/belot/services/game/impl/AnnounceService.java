package com.isoft.belot.services.game.impl;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.isoft.belot.domain.entities.Announce;
import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.Dealing;
import com.isoft.belot.domain.entities.Deck;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.Declaration;
import com.isoft.belot.domain.entities.enums.Team;
import com.isoft.belot.domain.models.view.belot.AnnounceViewModel;
import com.isoft.belot.domain.models.view.belot.PlayerActionViewModel;
import com.isoft.belot.game.InMemoryBelots;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.game.communication.server.enums.Header;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.repository.DealingRepository;
import com.isoft.belot.services.DealerServiceImpl;
import com.isoft.belot.services.game.announce.*;
import com.isoft.belot.services.interfaces.DealerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Stefan Ivanov
 * @author Aleksandar Todorov
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Slf4j
@Service
@Lazy
public class AnnounceService extends AbstractGameService
{
    private DealingRepository dealingRepository;

    private DealerService dealerService;

    public AnnounceService(SocketClientMessage message, String belotId)
    {
        super(message, belotId);
        if (!InMemoryBelots.openBelotsAnnounces.containsKey(belotId))
            InMemoryBelots.openBelotsAnnounces.put(belotId, new AnnounceStateContext());
    }

    @Override
    public void processMessage()
    {
        //checks whether the belot is not null
        if (!isBelotPresent())
            log.error("Belot is null, because belotId is wrong.");

        //take belot from memory
        initBelotFromMemory();

        BelotPlayer currentBelotPlayer = getCurrentBelotPlayer();
        //gets the last(current) Dealing of the belot
        Dealing lastDealing = Iterables.getLast(belot.getDealings());

        //try to convert payload from FRONT END to Announce object
        @Nullable Announce currentAnnounce = jsonParser.toObject(clientMessage.getPayload(), Announce.class);

        if (currentAnnounce != null)
        {
            currentAnnounce.setBelotPlayer(currentBelotPlayer);
            currentAnnounce.setCreated(LocalDateTime.now());
            currentAnnounce.setDealing(lastDealing);
        }

        //Incorrect Announce (client doing funny business)
        if (currentAnnounce == null || !isCorrectAnnounce(currentAnnounce))
        {
            errorResponse(jsonParser.toJson("Incorrect Announce data"), currentBelotPlayer.getUser().getUsername());
            AnnounceViewModel avm = createAnnounceViewModel(lastDealing, currentBelotPlayer);
            sendMessageToSpecific(Header.ANNOUNCE,
                    jsonParser.toJson(avm),
                    currentBelotPlayer.getUser().getUsername());
            return;
        }

        AnnounceStateContext ctx = InMemoryBelots.openBelotsAnnounces.get(belotId);
        AnnounceState currentState = ctx.getCurrentState();

        PlayerActionViewModel playerTurn = currentState.announce(currentAnnounce);
        currentState = ctx.getCurrentState();

        if (currentState instanceof FirstAnnounce)
        {
            initializeDoubleOptionsMap();
            changeDoubles(currentAnnounce, currentAnnounce.getBelotPlayer());
            newDealing(currentAnnounce);
        } else if (currentState instanceof NextAnnounce)
        {
            changeDoubles(currentAnnounce, currentAnnounce.getBelotPlayer());

            BelotPlayer nextPlayer = belot.getNext(currentBelotPlayer);
            lastDealing.getAnnounces().add(currentAnnounce);
            lastDealing.setAnnounces(lastDealing.getSortedAnnounces());
            AnnounceViewModel avm = createAnnounceViewModel(lastDealing, nextPlayer);

            //Send to everyone except the current user about the current user's action (announce/magic card)
            sendMessageToAllExcept(Header.PLAYER_ACTION,
                    jsonParser.toJson(playerTurn),
                    currentBelotPlayer.getUser().getUsername());
            //Send message only to the next player with available announces
            sendMessageToSpecific(Header.ANNOUNCE,
                    jsonParser.toJson(avm),
                    nextPlayer.getUser().getUsername());
        } else if (currentState instanceof GoToPlayPhase)
        {
            lastDealing.getAnnounces().add(currentAnnounce);
            lastDealing.setAnnounces(lastDealing.getSortedAnnounces());

            dealingRepository.saveAndFlush(lastDealing);
            dealerService.dealTheRestOfCards(belot);

            initializeMapsInMemory();

            for (BelotPlayer belotPlayer : belot.getBelotPlayers())
            {
                InMemoryBelots.playersCards.get(belotId).put(belotPlayer, belotPlayer.getCards());
                createAndSendGameInfo(belotPlayer);
            }
        }
    }

    private void initializeMapsInMemory()
    {
        //initialize roundCards Map for Play Phase
        InMemoryBelots.roundCards.put(belotId, new LinkedHashSet<>());

        //initialize teamsDeclarations Map for Play Phase
        Map<Team, List<Declaration>> teamsDeclarationsMap = new EnumMap<>(Team.class);
        teamsDeclarationsMap.put(Team.TEAM_ONE_FIRST, new ArrayList<>());
        teamsDeclarationsMap.put(Team.TEAM_ONE_SECOND, new ArrayList<>());
        teamsDeclarationsMap.put(Team.TEAM_TWO_FIRST, new ArrayList<>());
        teamsDeclarationsMap.put(Team.TEAM_TWO_SECOND, new ArrayList<>());
        InMemoryBelots.teamsDeclarations.put(belotId, teamsDeclarationsMap);

        //initialize playedCards and playersCards Maps
        InMemoryBelots.playedCards.put(belotId, new HashMap<>());
        InMemoryBelots.playersCards.put(belotId, new HashMap<>());
    }

    private boolean isCorrectAnnounce(Announce currentAnnounce)
    {
        @Nullable final Announce lastAnnounceNotPassAndDouble = currentAnnounce.getDealing().lastSuitAnnounce();

        final Set<AnnounceType> suits = Sets.newHashSet(AnnounceType.PASS, AnnounceType.CLUBS, AnnounceType.DIAMONDS,
                AnnounceType.HEARTS, AnnounceType.SPADES, AnnounceType.NO_TRUMPS, AnnounceType.ALL_TRUMPS);

        //whether this is first Announce and is one of the correct suits
        if (lastAnnounceNotPassAndDouble == null && suits.contains(currentAnnounce.getAnnounceType()))
            return true;
        else
        {
            boolean b1 = lastAnnounceNotPassAndDouble.getAnnounceType().order < currentAnnounce.getAnnounceType().order;
            boolean b2 = currentAnnounce.getAnnounceType().order == 0;
            boolean b3 = currentAnnounce.getAnnounceType() == AnnounceType.DOUBLE &&
                    InMemoryBelots.doublesOptions.get(belotId).get(currentAnnounce.getBelotPlayer())[0];
            boolean b4 = currentAnnounce.getAnnounceType() == AnnounceType.RE_DOUBLE &&
                    InMemoryBelots.doublesOptions.get(belotId).get(currentAnnounce.getBelotPlayer())[1];

            return b1 || b2 || b3 || b4;
        }
    }

    /**
     * stop this Dealing and start new Dealing with new Deck and deal 5 cards each
     * changes who play first in Memory
     */
    private void newDealing(Announce playerAnnounce)
    {
        Dealing lastDealing = playerAnnounce.getDealing();
        if (lastDealing != null)
        {
            lastDealing.getAnnounces().add(playerAnnounce);
            lastDealing.setAnnounces(lastDealing.getSortedAnnounces());
            dealingRepository.saveAndFlush(lastDealing);
        }

        Dealing dealing = new Dealing();
        //set belot_id for the new Dealing otherwise Hibernate throws Exception
        dealing.setBelot(belot);
        dealing.setCreated(LocalDateTime.now());
        belot.getDealings().add(dealing);

        Deck deck;
        deck = new Deck(belot);
        dealerService.clearCards(belot);
        dealerService.dealCards(belot, deck, 5);
        for (BelotPlayer player : belot.getBelotPlayers())
        {
            dealerService.orderCards(player);
            createAndSendGameInfo(player);
        }
        //save the Announce Phase and new Cards in the DB

        //save belot in Memory
        InMemoryBelots.allOpenBelots.put(belotId, belot);
        //clear Double and Redouble Map from in memory
        initializeDoubleOptionsMap();

        AnnounceViewModel avm = new AnnounceViewModel();
        //create view model with available announces for first player
        avm.setAvailableAnnounces(Arrays.stream(
                AnnounceType.values())
                .sorted((ann1, ann2) -> ann2.order - ann1.order)
                .collect(Collectors.toMap(e -> e, e -> e.order >= 0, (old, newVal) -> newVal, LinkedHashMap::new)));

        //change belotPlayerPlayFirst to next player in Memory
        InMemoryBelots.belotPlayerPlayFirst.put(belotId, belot.getNext(
                InMemoryBelots.belotPlayerPlayFirst.get(belotId)));
        //send message to this nex play that is his turn to announce the new Announce Phase
        sendMessageToSpecific(Header.ANNOUNCE,
                jsonParser.toJson(avm),
                InMemoryBelots.belotPlayerPlayFirst.get(belotId).getUser().getUsername());
    }

    /**
     * depends on what
     *
     * @param playerAnnounce     has been played from
     * @param currentBelotPlayer this method changes {@link InMemoryBelots}
     */
    private void changeDoubles(Announce playerAnnounce, BelotPlayer currentBelotPlayer)
    {
        Map<BelotPlayer, Boolean[]> doublesMap = new HashMap<>();
        Boolean[] doublesTeam1 = new Boolean[2];
        Boolean[] doublesTeam2 = new Boolean[2];

        if (!playerAnnounce.getAnnounceType().equals(AnnounceType.PASS) &&
                !playerAnnounce.getAnnounceType().equals(AnnounceType.DOUBLE) &&
                !playerAnnounce.getAnnounceType().equals(AnnounceType.RE_DOUBLE))
        {
            //canDouble and canRedouble for me and myTeammate = false
            //canDouble and canRedouble for opponents = true
            setDoubles(currentBelotPlayer, doublesMap, doublesTeam1, doublesTeam2, true, false);

        } else if (playerAnnounce.getAnnounceType().equals(AnnounceType.DOUBLE))
        {
            // canDouble and canRedouble for me and myTeammate = false
            // canDouble for opponents = false
            // canRedouble for opponents = true
            setDoubles(currentBelotPlayer, doublesMap, doublesTeam1, doublesTeam2, false, true);

        } else if (playerAnnounce.getAnnounceType().equals(AnnounceType.RE_DOUBLE))
        {
            // canDouble and canRedouble for all = false
            setDoubles(currentBelotPlayer, doublesMap, doublesTeam1, doublesTeam2, false, false);
        }
    }

    private void setDoubles(BelotPlayer currentBelotPlayer, Map<BelotPlayer, Boolean[]> doublesMap, Boolean[] doublesTeam1, Boolean[] doublesTeam2, boolean b, boolean b2)
    {
        doublesTeam1[0] = false;
        doublesTeam1[1] = false;
        doublesMap.put(currentBelotPlayer, doublesTeam1);
        doublesMap.put(belot.getTeamMate(currentBelotPlayer), doublesTeam1);

        doublesTeam2[0] = b;
        doublesTeam2[1] = b2;
        doublesMap.put(belot.getEnemies(currentBelotPlayer)[0], doublesTeam2);
        doublesMap.put(belot.getEnemies(currentBelotPlayer)[1], doublesTeam2);
        InMemoryBelots.doublesOptions.put(belotId, doublesMap);
    }

    private AnnounceViewModel createAnnounceViewModel(Dealing lastDealing, BelotPlayer player)
    {
        AnnounceViewModel avm = new AnnounceViewModel();
        avm.setAvailableAnnounces(getAvailableAnnounces(lastDealing, player));

        return avm;
    }

    private LinkedHashMap<AnnounceType, Boolean> getAvailableAnnounces(Dealing dealing, BelotPlayer player)
    {
        List<Announce> anns = dealing.getAnnounces().stream().filter(ann -> ann.getAnnounceType().order > 0).collect(Collectors.toList());
        AnnounceType lastAnnounce = AnnounceType.PASS;
        if (!anns.isEmpty())
            lastAnnounce = anns.get(anns.size() - 1).getAnnounceType();
        AnnounceType finalLastAnnounce = lastAnnounce;
        LinkedHashMap<AnnounceType, Boolean> map =
                Arrays.stream(AnnounceType.values())
                        .sorted((ann1, ann2) -> ann2.order - ann1.order)
                        .collect(Collectors.toMap(
                                e -> e,
                                ann -> ann.order > finalLastAnnounce.order || ann.order <= 0,
                                (oldVal, newVal) -> oldVal,
                                LinkedHashMap::new));
        map.put(AnnounceType.DOUBLE, InMemoryBelots.doublesOptions.get(belotId).get(player)[0]);
        map.put(AnnounceType.RE_DOUBLE, InMemoryBelots.doublesOptions.get(belotId).get(player)[1]);

        return map;
    }

    @Override
    @Autowired
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    @Autowired
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

    @Override
    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }

    @Autowired
    public void setDealingRepository(DealingRepository dealingRepository)
    {
        this.dealingRepository = dealingRepository;
    }

    @Autowired
    public void setDealerService(DealerServiceImpl dealerServiceImpl)
    {
        this.dealerService = dealerServiceImpl;
    }
}