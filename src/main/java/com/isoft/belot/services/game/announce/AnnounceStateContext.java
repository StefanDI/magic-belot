package com.isoft.belot.services.game.announce;

import lombok.Getter;

/**
 * @author Aleksandar Todorov
 */
@Getter
public class AnnounceStateContext {

    private AnnounceState newDealing;
    private AnnounceState nextAnnounce;
    private AnnounceState goToPlay;
    private AnnounceState currentState;

    public AnnounceStateContext() {
        this.newDealing = new FirstAnnounce(this);
        this.nextAnnounce = new NextAnnounce(this);
        this.goToPlay = new GoToPlayPhase(this);
        this.currentState = this.newDealing;
    }

    public void setCurrentState(AnnounceState currentState) {
        this.currentState = currentState;
    }
}
