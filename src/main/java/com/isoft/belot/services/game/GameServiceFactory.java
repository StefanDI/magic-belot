package com.isoft.belot.services.game;

import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.services.game.impl.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * Factory for lazily instantiating prototype beans of type {@link AbstractGameService}
 * TODO: Change it to be {@link com.isoft.belot.services.interfaces.MessageHandlingService}
 *
 * @author Stefan Ivanov
 */
@Component
@Lazy
public class GameServiceFactory
        extends AbstractFactoryBean<AbstractGameService>
        implements ApplicationContextAware
{
    private ApplicationContext applicationContext;
    private SocketClientMessage clientMessage;
    private String belotId;

    public GameServiceFactory()
    {
        setSingleton(false);
    }

    @Override
    public Class<?> getObjectType()
    {
        return AbstractGameService.class;
    }

    @Override
    public final AbstractGameService createInstance() throws Exception
    {
        if (this.clientMessage == null)
            throw new IllegalStateException("Could not create game service, client message is null");
        final AbstractGameService service = getService(clientMessage, belotId);
        if (service != null)
        {
            applicationContext
                    .getAutowireCapableBeanFactory()
                    .autowireBean(service);
        }

        return service;
    }

    public void setClientMessage(SocketClientMessage clientMessage)
    {
        this.clientMessage = clientMessage;
    }

    public void setBelotId(String belotId)
    {
        this.belotId = belotId;
    }

    /**
     * Returns a specific game service, depending on the message type
     *
     * @param message the socket message sent from client
     * @return the appropriate GameService implementation
     * and null if message type is invalid
     * TODO: ErrorGameService maybe
     */
    private AbstractGameService getService(SocketClientMessage message, String belotId)
    {
        switch (message.getType())
        {
            case CREATE_PRIVATE_GAME:
                return new CreateGameService(message, belotId);
            case JOIN_PRIVATE_GAME:
                return new JoinPrivateGameService(message, belotId);
            case SEARCH_RANKED:
                return new SearchRankedService(message, belotId);
            case CANCEL_SEARCH:
                return new CancelSearchService(message, belotId);
            case READY:
                return new GameInitializerService(message, belotId);
            case ANNOUNCE:
                return new AnnounceService(message, belotId);
            case PLAY_CARD:
                return new PlayService(message,belotId);
            case LEAVE_GAME:
                break;
            default:
                return null;
        }
        return null;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }
}
