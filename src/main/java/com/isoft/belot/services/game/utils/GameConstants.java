package com.isoft.belot.services.game.utils;

/**
 * @author Stefan Ivanov
 */
public final class GameConstants
{
    public static final int NEEDED_PLAYERS_TO_START_A_GAME = 4;

    private GameConstants(){};
}
