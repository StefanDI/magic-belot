package com.isoft.belot.services.game.announce;

import com.isoft.belot.domain.entities.Announce;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.MagicCard;
import com.isoft.belot.domain.models.view.belot.PlayerActionViewModel;

/**
 * @author Stefan Ivanov
 */
public interface AnnounceState
{
    PlayerActionViewModel announce(Announce playerAnnounce);

    default PlayerActionViewModel createViewModel(Announce playerAnnounce)
    {
        PlayerActionViewModel turnInfo = new PlayerActionViewModel();
        turnInfo.setUsername(playerAnnounce.getBelotPlayer().getUser().getUsername());
        if (playerAnnounce.getAnnounceType() != null)
            turnInfo.getTurnInfo().put(PlayerActionViewModel.TurnType.ANNOUNCE, playerAnnounce.getAnnounceType().toString());
        if (playerAnnounce.getMagicCard() != null)
            turnInfo.getTurnInfo().put(PlayerActionViewModel.TurnType.MAGIC_CARD_PLAYED, playerAnnounce.getMagicCard().toString());

        return turnInfo;
    }

    default boolean announcePassAndBlizzardStorm(Announce playerAnnounce)
    {
        return playerAnnounce.isMagicCardPlayed() &&
                playerAnnounce.getMagicCard() == MagicCard.BLIZZARD_STORM &&
                playerAnnounce.getAnnounceType() == AnnounceType.PASS;
    }
    default boolean announceSuitAndBlizzardStorm(Announce playerAnnounce)
    {
        return playerAnnounce.isMagicCardPlayed() &&
                playerAnnounce.getMagicCard() == MagicCard.BLIZZARD_STORM &&
                playerAnnounce.getAnnounceType() != AnnounceType.PASS;
    }
}
