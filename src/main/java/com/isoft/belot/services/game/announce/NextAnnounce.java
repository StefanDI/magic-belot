package com.isoft.belot.services.game.announce;

import com.isoft.belot.domain.entities.Announce;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.models.view.belot.PlayerActionViewModel;

/**
 * @author Stefan Ivanov
 */
public class NextAnnounce implements AnnounceState {

    private final AnnounceStateContext announceContext;

    public NextAnnounce(AnnounceStateContext announceContext) {
        this.announceContext = announceContext;
    }

    @Override
    public PlayerActionViewModel announce(Announce playerAnnounce) {
        if (announcePassAndBlizzardStorm(playerAnnounce))
            announceContext.setCurrentState(announceContext.getNewDealing());
        else if (announceSuitAndBlizzardStorm(playerAnnounce))
            announceContext.setCurrentState(announceContext.getGoToPlay());
        else
            handleAnnounce(playerAnnounce);

        return createViewModel(playerAnnounce);
    }

    private void handleAnnounce(Announce playerAnnounce) {
        if (playerAnnounce.getDealing().threeConsecutivePass())
            announceContext.setCurrentState(announceContext.getNewDealing());
        else if (playerAnnounce.getAnnounceType() == AnnounceType.PASS && playerAnnounce.getDealing().twoConsecutivePass())
            announceContext.setCurrentState(announceContext.getGoToPlay());
        else
            announceContext.setCurrentState(announceContext.getNextAnnounce());
    }
}
