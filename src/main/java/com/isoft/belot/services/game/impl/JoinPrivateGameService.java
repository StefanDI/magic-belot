package com.isoft.belot.services.game.impl;

import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.Card;
import com.isoft.belot.domain.entities.Deck;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.MagicCard;
import com.isoft.belot.domain.models.view.belot.AnnounceViewModel;
import com.isoft.belot.domain.models.view.belot.PlayerActionViewModel;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.game.communication.server.enums.Header;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.services.DealerServiceImpl;
import com.isoft.belot.services.interfaces.DealerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * TODO: This class logic needs rework
 *
 * @author Stefan Ivanov
 * TODO: this is not working, remake
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
@Lazy
public class JoinPrivateGameService extends AbstractGameService
{
    private DealerService dealerService;

    public JoinPrivateGameService(SocketClientMessage message,
                                  String belot)
    {
        super(message, belot);
    }


    @Override
    public void processMessage()
    {
        User u = clientMessage.getSender();

        AnnounceViewModel availableAnnounces = new AnnounceViewModel();
        availableAnnounces.setAvailableAnnounces(
                Arrays.stream(
                        AnnounceType.values())
                        .sorted(Comparator.comparingInt(announceType -> announceType.order))
                        .collect(Collectors.toMap(e -> e, e -> true, (old, newVal) -> newVal, LinkedHashMap::new))
        );
        availableAnnounces.setAvailableMagicCards(Arrays.stream(MagicCard.values())
                .collect(Collectors.toMap(e -> e, e -> true)));

        Set<Card> cards = new HashSet<>();
        Deck d = new Deck();
        for (int i = 0; i < 5; i++)
        {
            cards.add(d.drawCard());
        }
        cards = dealerService.orderCards(cards);

        PlayerActionViewModel t = new PlayerActionViewModel();
        t.setUsername("admin");
        t.getTurnInfo().put(PlayerActionViewModel.TurnType.ANNOUNCE, "PASS");
        sendMessageToSpecific(Header.CARDS, jsonParser.toJson(cards), clientMessage.getSender().getUsername());
        sendMessageToSpecific(Header.PLAYER_ACTION, jsonParser.toJson(t), clientMessage.getSender().getUsername());


        sendMessageToSpecific(Header.ANNOUNCE, jsonParser.toJson(availableAnnounces), clientMessage.getSender().getUsername());
//        if (isBelotPresent())
//        {
//            errorResponse("There is no such belot!", u.getUsername());
//            return;
//        }
//
//        if (isUserInAnotherGame(u))
//        {
//            errorResponse("Player already in game", u.getUsername());
//            return;
//        }
//        BelotPlayer player = new BelotPlayer(u, belot);
//        if (!canPlay(player))
//        {
//            errorResponse("Game is full", u.getUsername());
//            return;
//        }
//
//        belot.getBelotPlayers().add(player);
//        //TODO: change logic if someone left and was previously another team, this will go wrong
//        switch (belot.getBelotPlayers().size())
//        {
//            case 2:
//                player.setTeam(Team.TEAM_TWO_FIRST);
//                break;
//            case 3:
//                player.setTeam(Team.TEAM_ONE_SECOND);
//                break;
//            case 4:
//                player.setTeam(Team.TEAM_TWO_SECOND);
//                break;
//        }
//
//        if (belot.getBelotPlayers().size() == 4 &&
//                belot.getGameStatus().equals(GameStatus.WAITS_FOR_PLAYERS))
//        {
//            belot.readyToStart();
//            BelotPlayer[] belotPlayers = belot.getBelotPlayers().toArray(new BelotPlayer[0]);
//            Deck deck = new Deck();
//            belot.dealCards(deck,5);
//        }
    }

    @Override
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

    @Override
    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }

    private boolean canPlay(BelotPlayer player)
    {
        return belot.getBelotPlayers().size() <= 4 &&
                !belot.getBelotPlayers().contains(player);
    }

    @Autowired
    public void setDealerService(DealerServiceImpl dealerServiceImpl)
    {
        this.dealerService = dealerServiceImpl;
    }

}
