package com.isoft.belot.services.game.announce;

import com.isoft.belot.domain.entities.Announce;
import com.isoft.belot.domain.models.view.belot.PlayerActionViewModel;

/**
 * @author Stefan Ivanov
 */
public class GoToPlayPhase implements AnnounceState
{
    private final AnnounceStateContext announceContext;

    public GoToPlayPhase(AnnounceStateContext announceContext)
    {
        this.announceContext = announceContext;
    }

    @Override
    public PlayerActionViewModel announce(Announce playerAnnounce)
    {
        //No announce should be accepted in this state
        return null;
    }
}
