package com.isoft.belot.services.game.impl;

import com.isoft.belot.domain.entities.Belot;
import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.domain.models.view.belot.BelotViewModel;
import com.isoft.belot.game.InMemoryBelots;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.game.communication.server.SocketServerMessage;
import com.isoft.belot.game.communication.server.enums.Header;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.services.interfaces.MessageHandlingService;
import com.isoft.belot.util.JsonParser;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This is the abstract implementation of MessageHandlingService
 * It is designed to work with belot game, if another game is added we either need an abstraction above this
 * or another AbstractGameService for other game
 * Contains protected util methods for successors
 * and abstract setters for autowiring (due to the way this is instantiated, with factory)
 *
 * @author Stefan Ivanov
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Component
public abstract class AbstractGameService implements MessageHandlingService
{
    public static final String CLIENT_SUBSCRIBE_URL = "/topic/belot-game";

    protected final SocketClientMessage clientMessage;
    protected final String belotId;

    protected Belot belot;
    /**
     * would this be a new instance when another 4 players create a new Belot ? - no
     */

    protected BelotRepository belotRepository;
    protected BelotPlayerRepository playerRepository;
    protected SimpMessagingTemplate messagingTemplate;
    protected final JsonParser jsonParser;

    protected AbstractGameService(SocketClientMessage message,
                                  String belotId)
    {
        this.clientMessage = message;
        this.belotId = belotId;
        this.jsonParser = new JsonParser();
    }

    /**
     * @return true if the belot is present in the database
     */
    protected boolean isBelotPresent()
    {
        Optional<Belot> optionalBelot = this.belotRepository.findById(belotId);
        return optionalBelot.isPresent();
    }

    /**
     * Init belot from database, should be used only after {@link #isBelotPresent()} check
     */
    protected void initBelotFromDB()
    {
        Optional<Belot> optionalBelot = this.belotRepository.findById(belotId);

        //noinspection OptionalGetWithoutIsPresent
        this.belot = optionalBelot.get();
    }

    /**
     * Load the belot from in memory, and if it fails, load this.belot
     * TODO: this.belot is most certainly null, needs reworking
     */
    protected void initBelotFromMemory()
    {
        this.belot = InMemoryBelots.allOpenBelots.getOrDefault(belotId, belot);
    }

    /**
     * sends an error message to specific user
     *
     * @param message  error text
     * @param username the username of the player
     */
    protected void errorResponse(String message, String username)
    {
        SocketServerMessage serverMessage = new SocketServerMessage();
        serverMessage.setHeader(Header.ERROR);
        serverMessage.setMessage(message);

        messagingTemplate.convertAndSendToUser(username, CLIENT_SUBSCRIBE_URL, serverMessage);
    }

    protected void successResponse(String message, String username)
    {
        SocketServerMessage serverMessage = new SocketServerMessage();
        serverMessage.setHeader(Header.SUCCESS);
        serverMessage.setMessage(message);

        messagingTemplate.convertAndSendToUser(username, CLIENT_SUBSCRIBE_URL, serverMessage);
    }

    /**
     * Send a message to a specific player
     *
     * @param header   header of the message
     * @param message  the message
     * @param username the username of the player
     */
    protected void sendMessageToSpecific(Header header, String message, String username)
    {
        SocketServerMessage serverMessage = new SocketServerMessage();
        serverMessage.setHeader(header);
        serverMessage.setMessage(message);
        serverMessage.setSendToSpecific(username);

        messagingTemplate.convertAndSendToUser(username, CLIENT_SUBSCRIBE_URL, serverMessage);
    }

    /**
     * Send message to all the players subscribed to the current belot
     *
     * @param header  the header or the message
     * @param message the message
     */
    protected void sendMessageToAllPlayers(Header header, String message)
    {
        SocketServerMessage serverMessage = new SocketServerMessage();
        serverMessage.setHeader(header);
        serverMessage.setMessage(message);

        messagingTemplate.convertAndSend(CLIENT_SUBSCRIBE_URL + "/" + belotId, serverMessage);
    }

    /**
     * Send message to everyone except one user
     *
     * @param username the user to whom the message should not be send
     * @param header   the header
     * @param message  the message
     */
    protected void sendMessageToAllExcept(Header header, String message, String username)
    {
        SocketServerMessage serverMessage = new SocketServerMessage();
        serverMessage.setHeader(header);
        serverMessage.setMessage(message);

        for (BelotPlayer belotPlayer : belot.getBelotPlayers())
        {
            if (!belotPlayer.getUser().getUsername().equals(username))
                messagingTemplate.convertAndSendToUser(belotPlayer.getUser().getUsername(), CLIENT_SUBSCRIBE_URL, serverMessage);
        }
    }


    /**
     * Checks if there is another {@link BelotPlayer} with the same property {@link BelotPlayer#user}
     * this means the player is engaged in another game
     *
     * @param u the user
     * @return true if the user is in another game
     */
    @SuppressWarnings("JavadocReference")
    protected boolean isUserInAnotherGame(User u)
    {
        //  return playerRepository.findByUser(u) != null;
        return false; //TODO change it back because user could be only in one belot
    }

    /**
     * send info to FRONT END once every Dealing when 5 cards are dealt.
     * @param player
     */
    protected void createAndSendGameInfo(BelotPlayer player)
    {
        BelotViewModel bvm = new BelotViewModel();

        //Send information about the players
        BelotPlayer next = belot.getNext(player);
        BelotPlayer partner = belot.getTeamMate(player);
        BelotPlayer left =
                belot.getBelotPlayers().stream()
                        .filter(p -> !p.equals(next) && !p.equals(partner) && !p.equals(player))
                        .findFirst().orElseThrow(() -> new IllegalStateException("Left player not found"));
        bvm.setRight(next);
        bvm.setLeft(left);
        bvm.setPartner(partner);

        sendMessageToSpecific(Header.CARDS, jsonParser.toJson(player.getCards()), player.getUser().getUsername());
        sendMessageToSpecific(Header.PLAYERS_INFO, jsonParser.toJson(bvm), player.getUser().getUsername());
    }

    protected BelotPlayer getCurrentBelotPlayer()
    {
        return belot.getBelotPlayers().stream()
                .filter(
                        p -> p.getUser().getId().equals(clientMessage.getSender().getId()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("There is no such player in this belot"));
    }

    protected void initializeDoubleOptionsMap()
    {
        Map<BelotPlayer, Boolean[]> map = new HashMap<>();
        for (BelotPlayer belotPlayer : belot.getBelotPlayers())
        {
            map.put(belotPlayer, new Boolean[]{false, false});
        }
        InMemoryBelots.doublesOptions.put(belot.getId(), map);
    }

    /**
     * For autowiring in successors
     */
    public abstract void setBelotRepository(BelotRepository belotRepository);

    public abstract void setPlayerRepository(BelotPlayerRepository playerRepository);

    public abstract void setMessagingTemplate(SimpMessagingTemplate messagingTemplate);
}
