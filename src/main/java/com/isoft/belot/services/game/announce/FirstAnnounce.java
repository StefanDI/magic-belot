package com.isoft.belot.services.game.announce;

import com.isoft.belot.domain.entities.Announce;
import com.isoft.belot.domain.models.view.belot.PlayerActionViewModel;

/**
 * @author Stefan Ivanov
 */
public class FirstAnnounce implements AnnounceState
{
    private final AnnounceStateContext stateContext;

    public FirstAnnounce(AnnounceStateContext stateContext)
    {
        this.stateContext = stateContext;
    }

    @Override
    public PlayerActionViewModel announce(Announce playerAnnounce)
    {
        if (announcePassAndBlizzardStorm(playerAnnounce))
            stateContext.setCurrentState(stateContext.getNewDealing());
        else if (announceSuitAndBlizzardStorm(playerAnnounce))
            stateContext.setCurrentState(stateContext.getGoToPlay());
        else
            stateContext.setCurrentState(stateContext.getNextAnnounce());

        return createViewModel(playerAnnounce);
    }
}
