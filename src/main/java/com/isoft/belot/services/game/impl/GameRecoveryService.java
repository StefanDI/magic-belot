package com.isoft.belot.services.game.impl;

import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * Check if the player is already a game, and recover it
 * @author Stefan Ivanov
 */
@Service
@Lazy
public class GameRecoveryService extends AbstractGameService {

    protected GameRecoveryService(SocketClientMessage message, String belotId) {
        super(message, belotId);
    }

    @Override
    public void processMessage() {
        /**
         * TODO:
         *  Check if belotId is null ( we saved belot in session/local storage,
         *  and try to reconstruct it from there )
         *  if its not null, take the current game progress from inMemoryBelots or DB
         *  and return message
         *  Else: belot is null, check if the userId is associated with any belot
         */
    }

    @Override
    @Autowired
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    @Autowired
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

    @Override
    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }
}
