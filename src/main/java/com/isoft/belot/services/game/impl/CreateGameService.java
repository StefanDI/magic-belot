package com.isoft.belot.services.game.impl;

import com.isoft.belot.domain.entities.Belot;
import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.domain.entities.enums.Team;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author Stefan Ivanov
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
@Lazy
public class CreateGameService extends AbstractGameService
{

    public CreateGameService(SocketClientMessage message,
                             String belotId)
    {
        super(message, belotId);
    }

    @Override
    public void processMessage()
    {
        User u = clientMessage.getSender();
        if(isUserInAnotherGame(u))
        {
            errorResponse("Player already in game", u.getUsername());
            return;
        }
        //TODO: rework this
        belot = new Belot();

        BelotPlayer belotPlayer = new BelotPlayer(u, belot);

        belot.getBelotPlayers().add(belotPlayer);
        belotPlayer.setTeam(Team.TEAM_ONE_FIRST);
        belotPlayer.setBelot(belot);

        belotRepository.saveAndFlush(belot);
        playerRepository.saveAndFlush(belotPlayer);

//        serverMessage.setMessage(belot.getId());
//        serverMessage.setHeader(Header.GAME_STARTED);
    }


    @Override
    @Autowired
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    @Autowired
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

    @Override
    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }
}
