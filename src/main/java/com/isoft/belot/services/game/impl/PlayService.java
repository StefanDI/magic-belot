package com.isoft.belot.services.game.impl;

import com.google.common.collect.Iterables;
import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.Card;
import com.isoft.belot.domain.entities.Dealing;
import com.isoft.belot.domain.entities.Play;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.Team;
import com.isoft.belot.game.InMemoryBelots;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.game.communication.server.enums.Header;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.repository.DealingRepository;
import com.isoft.belot.services.DealerServiceImpl;
import com.isoft.belot.services.DeclarationManager;
import com.isoft.belot.services.interfaces.DealerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Aleksandar Todorov
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Slf4j
@Service
@Lazy
public class PlayService extends AbstractGameService
{
    private DealingRepository dealingRepository;
    private DealerService dealerService;
    private DeclarationManager declarationManager;

    public PlayService(SocketClientMessage message, String belotId)
    {
        super(message, belotId);
    }

    /**
     * take the belot and lastDealing from DB
     * take currentPlay from FRONT END
     * check if currentPlay is correct(not null)
     * check if this card is valid (is it belonged to him ?)
     * what is the current announceType
     * check if this card could be played. 1. it should be from same Suit. 2. stronger if TRUMP. 3. TRUMP if possible. 4. any other random card
     * check if this is one of the 1st 4 Plays -> check for Declarations and automatically declare them.
     * check if the card is K or Q -> could he declare Belot and automatically declare it.
     * add the card InMemory
     * delete the card from currentPlayer cards
     * check if this is 4rd Card -> check who is the winner from the 4 cards -> check if he is from TEAM_FIRST
     * redirect the turn to the winner or if this is 32th card played -> redirect to PointsService
     */
    @Override
    public void processMessage()
    {
        //checks whether the belot is not null
        if (!isBelotPresent())
            log.error("Belot is null, because belotId is wrong.");

        //take belot from DB
        initBelotFromDB();

        BelotPlayer currentBelotPlayer = getCurrentBelotPlayer();

        //set his cards from Memory(cause we don't save cards in DB)
        currentBelotPlayer.setCards(InMemoryBelots.playersCards.get(belotId).get(currentBelotPlayer));

        //take the last(current) Dealing of belot from DB. It is already saved in DB from AnnounceService
        //so it has id. If we get it from InMemory it could be a problem when we trying to save it in the DB.
        Dealing lastDealing = dealingRepository.getFirstByBelotOrderByCreatedDesc(belot);

        //what is the current announceType
        AnnounceType roundAnnounceType = dealerService.determineRoundAnnounceType(lastDealing);

        //try to take currentPlay from FRONT END
        @Nullable Play currentPlay = jsonParser.toObject(clientMessage.getPayload(), Play.class);

        //check for Incorrect Play
        if (currentPlay == null || //client doing funny business
                !currentBelotPlayer.getCards().contains(currentPlay.getCard()) ||  //check if this card is valid (is it belonged to player?)
                !isCardPlayable(currentPlay.getCard(), roundAnnounceType, currentBelotPlayer)) //check if this card could be played.
        {
            errorResponse(jsonParser.toJson("Incorrect Play data"), currentBelotPlayer.getUser().getUsername());
            sendMessageToSpecific(Header.YOUR_TURN,
                    "It is still your turn",
                    currentBelotPlayer.getUser().getUsername());
            return;
        }

        //check if this is one of the 1st 4 Plays -> check for Declarations and automatically declare them
        //as putting them in InMemoryBelots.teamsDeclarations
        declarationManager.checkForDeclarations(belotId, currentBelotPlayer, roundAnnounceType);

        //check if the card is K or Q -> could he declare Belot and automatically declare it.
        if (currentBelotPlayer.getCards().size() > 1)
            declarationManager.checkForBelotDeclaration(belotId, currentBelotPlayer, currentPlay.getCard(), roundAnnounceType);

        //add the card in Memory (in roundCards and playedCards)
        InMemoryBelots.roundCards.get(belotId).add(currentPlay.getCard());
        InMemoryBelots.playedCards.get(belotId).put(currentPlay.getCard(), currentBelotPlayer);

        //delete the card in Memory from currentPlayer cards
        InMemoryBelots.playersCards.get(belotId).get(currentBelotPlayer).remove(currentPlay.getCard());

        //check if this is 4rd Card of the round
        if (InMemoryBelots.roundCards.get(belotId).size() == 4)
        {
            //check who is the winner from the 4 cards
            Card winningCard = dealerService.whichCardWins(InMemoryBelots.roundCards.get(belotId), roundAnnounceType);
            //who played the winning card
            BelotPlayer winnerPlayer = InMemoryBelots.playedCards.get(belotId).get(winningCard);

            //check if the winner is from TEAM_FIRST(we need to keep only one team winning cards)
            if (winnerPlayer.getTeam().equals(Team.TEAM_ONE_FIRST) || winnerPlayer.getTeam().equals(Team.TEAM_ONE_SECOND))
                InMemoryBelots.firstTeamCardsWon.get(belotId).addAll(InMemoryBelots.roundCards.get(belotId));

            //clear the roundCards
            InMemoryBelots.roundCards.get(belotId).clear();

            //if this is 32th card played -> redirect to PointsService
            if (InMemoryBelots.playersCards.get(belotId).get(currentBelotPlayer).isEmpty())
            {
                //TODO redirect to PointsService
            } else //redirect the turn to the winner
            {
                sendMessageToSpecific(Header.YOUR_TURN, "", winnerPlayer.getUser().getUsername());
            }
        }

        //TODO go to next player anticlockwise
    }

    //check if this card could be played. 1. it should be from same Suit. 2. stronger if TRUMP. 3. TRUMP . 4. any other random card
    private boolean isCardPlayable(Card currentCard, AnnounceType roundAnnounceType, BelotPlayer currentBelotPlayer)
    {
        Set<Card> possibleCards;
        Set<Card> tempCards;
        Card firstCard;
        Set<Card> playedCards = InMemoryBelots.roundCards.get(belotId);

        //whether your card is first this Round
        if (!playedCards.isEmpty())
        {
            firstCard = Iterables.getFirst(playedCards, null);
            assert firstCard != null;

            //1. check whether you have cards from the same Suit as First Card.
            possibleCards = currentBelotPlayer
                    .getCards()
                    .stream()
                    .filter(card -> card.getSuit().equals(firstCard.getSuit()))
                    .collect(Collectors.toSet());

            if (!possibleCards.isEmpty())
            {   //2. should be stronger if TRUMP.
                if (roundAnnounceType.equals(AnnounceType.ALL_TRUMPS)
                        || firstCard.getSuit().name().equals(roundAnnounceType.name()))
                {
                    int maxTrumpPoints = dealerService
                            .getStrongestTrumpCard(playedCards)
                            .getCardValue()
                            .getTrumpPoints();

                    tempCards = possibleCards
                            .stream()
                            .filter(card -> card.getCardValue().getTrumpPoints() > maxTrumpPoints)
                            .collect(Collectors.toSet());

                    if (!tempCards.isEmpty())
                        return tempCards.contains(currentCard);
                }

                return possibleCards.contains(currentCard);

            } else
            {
                //should play TRUMP if possible (да цакам)
                if (!roundAnnounceType.equals(AnnounceType.ALL_TRUMPS) &&
                        !roundAnnounceType.equals(AnnounceType.NO_TRUMPS) &&
                        !firstCard.getSuit().name().equals(roundAnnounceType.name()))
                {
                    possibleCards = currentBelotPlayer
                            .getCards()
                            .stream()
                            .filter(card -> card.getSuit().name().equals(roundAnnounceType.name()))
                            .collect(Collectors.toSet());

                    //do you have TRUMP for цакане
                    if (!possibleCards.isEmpty())
                    {
                        //някой цакал ли е вече?
                        if (playedCards.stream().anyMatch(card -> roundAnnounceType.name().equals(card.getSuit().name())))
                        {
                            int maxTrumpPoints = dealerService
                                    .getStrongestTrumpCard(playedCards, roundAnnounceType)
                                    .getCardValue()
                                    .getTrumpPoints();

                            tempCards = possibleCards
                                    .stream()
                                    .filter(card -> card.getCardValue().getTrumpPoints() > maxTrumpPoints)
                                    .collect(Collectors.toSet());

                            //имам по-голям коз
                            if (!tempCards.isEmpty())
                                return tempCards.contains(currentCard);
                            else
                                return true;
                        } else
                            return possibleCards.contains(currentCard);
                    }
                }
            }
        }

        //play any random card
        return true;
    }

    @Override
    @Autowired
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    @Autowired
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

    @Override
    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }

    @Autowired
    public void setDealingRepository(DealingRepository dealingRepository)
    {
        this.dealingRepository = dealingRepository;
    }

    @Autowired
    public void setDealerService(DealerServiceImpl dealerServiceImpl)
    {
        this.dealerService = dealerServiceImpl;
    }

    @Autowired
    public void setDeclarationManager(DeclarationManager declarationManager)
    {
        this.declarationManager = declarationManager;
    }
}
