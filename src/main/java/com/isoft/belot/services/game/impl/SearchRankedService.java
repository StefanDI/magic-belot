package com.isoft.belot.services.game.impl;

import com.isoft.belot.domain.entities.User;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.services.game.GameMatchingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Stefan Ivanov
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
@Lazy
public class SearchRankedService extends AbstractGameService
{
    private GameMatchingService gameMatchingService;

    public SearchRankedService(SocketClientMessage message, String belotId)
    {
        super(message, belotId);
    }

    @Override
    public void processMessage()
    {

        User u = clientMessage.getSender();
        if (isUserInAnotherGame(u))
        {
            errorResponse("User is already playing", u.getUsername());
            return;
        }

        this.gameMatchingService.addUserToQueue(u);

    }

    @Autowired
    public void setGameMatchingService(GameMatchingService gameMatchingService)
    {
        this.gameMatchingService = gameMatchingService;
    }

    @Override
    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }

    @Override
    @Autowired
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    @Autowired
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

}
