package com.isoft.belot.services.game.impl;

import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.Deck;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.MagicCard;
import com.isoft.belot.domain.models.view.belot.AnnounceViewModel;
import com.isoft.belot.game.InMemoryBelots;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.game.communication.server.enums.Header;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.services.DealerServiceImpl;
import com.isoft.belot.services.game.utils.GameConstants;
import com.isoft.belot.services.interfaces.DealerService;
import com.isoft.belot.util.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Holds a map of <belot , List<acceptedPlayers>>, when the needed amount of players for a game is reached
 * initializes the game
 * TODO: Set a timeout for ready check and delete game and BelotPlayers
 * also send message to clients that they must send "SEARCH_RANKED" message again
 * TODO: Save belot state (players with teams, cards etc) in the database
 * TODO: Send {@link com.isoft.belot.game.communication.server.SocketServerMessage}
 *  with JSON formatted Announce options to the first player(maybe AnnounceViewModel class)
 *
 * @author Stefan Ivanov
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
@Lazy
public class GameInitializerService extends AbstractGameService
{
    //A list with all the belots and a list of players who sent "READY" response
    private static final Map<String, List<Boolean>> pendingBelots = new HashMap<>();

    private DealerService dealerService;

    public GameInitializerService(SocketClientMessage message, String belotId)
    {
        super(message, belotId);
    }

    @Override
    public void processMessage()
    {
        pendingBelots.computeIfAbsent(belotId, k -> new ArrayList<>());

        pendingBelots.get(belotId).add(true);

        if (pendingBelots.get(belotId).size() == GameConstants.NEEDED_PLAYERS_TO_START_A_GAME)
        {
            if (!isBelotPresent())
            {
                throw new NullPointerException("Belot is null, because belotId is wrong.");
            }
            initBelotFromDB();
            belot.assignTeams();
            Deck deck = new Deck(belot);
            dealerService.dealCards(belot, deck, 5);
            for (BelotPlayer belotPlayer : belot.getBelotPlayers())
            {
                dealerService.orderCards(belotPlayer);
            }
            initializeDoubleOptionsMap();

            //Save the initial belot state in DB and to the in memory belots
            InMemoryBelots.allOpenBelots.put(belotId, belot);

            InMemoryBelots.belotPlayerPlayFirst.put(belotId, belot.getFirst());

            belotRepository.saveAndFlush(belot);

            String firstUser = belot.getFirst().getUser().getUsername();

            JsonParser jsonParser = new JsonParser();

            /** Game is ready, tell everyone to subscribe to the specific belot*/
            for (BelotPlayer player : belot.getBelotPlayers())
            {
                sendMessageToSpecific(Header.SUBSCRIBE_TO,
                        belot.getId(),
                        player.getUser().getUsername());

                createAndSendGameInfo(player);
            }

            AnnounceViewModel availableAnnounces = new AnnounceViewModel();
            availableAnnounces.setAvailableAnnounces(
                    Arrays.stream(
                            AnnounceType.values())
                            .sorted((ann1, ann2) -> ann2.order - ann1.order)
                            .collect(Collectors.toMap(e -> e, e -> e.order >= 0, (old, newVal) -> newVal, LinkedHashMap::new)));

            availableAnnounces.setAvailableMagicCards(Arrays.stream(MagicCard.values())
                    .collect(Collectors.toMap(e -> e, e -> true)));

            sendMessageToSpecific(Header.ANNOUNCE, jsonParser.toJson(availableAnnounces), firstUser);

        }
    }

    @Override
    @Autowired
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    @Autowired
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

    @Override
    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }

    @Autowired
    public void setDealingRepository(DealerServiceImpl dealerServiceImpl)
    {
        this.dealerService = dealerServiceImpl;
    }
}
