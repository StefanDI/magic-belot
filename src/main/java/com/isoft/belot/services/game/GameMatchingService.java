package com.isoft.belot.services.game;

import com.isoft.belot.domain.entities.Belot;
import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.domain.entities.enums.GameType;
import com.isoft.belot.game.communication.server.SocketServerMessage;
import com.isoft.belot.game.communication.server.enums.Header;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.repository.UserRepository;
import com.isoft.belot.services.game.impl.AbstractGameService;
import com.isoft.belot.util.DummyUsersGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.isoft.belot.services.game.utils.GameConstants.NEEDED_PLAYERS_TO_START_A_GAME;

/**
 * This class is actually a daemon thread(low priority thread working in the background, during the whole lifecycle of the app)
 * implements {@link InitializingBean} for {@link #afterPropertiesSet()} method so when i start the thread all the autowired dependencies are set
 * <p>
 * implements {@link DisposableBean} so spring can manage its lifecycle (and prevent memory leak)
 * TODO: since its a daemon thread, maybe we dont need spring to manage its lifecycle, needs research and testing
 *
 * @author Stefan Ivanov
 * @see <a href="https://stackoverflow.com/questions/30726189/spring-why-is-afterpropertiesset-of-initializingbean-needed-when-there-are-st">
 * Info on InitilizingBean</a>
 * @see <a href="https://www.javatpoint.com/daemon-thread">Info on daemon threads</a>
 */
@Slf4j
@Service
public class GameMatchingService implements InitializingBean, Runnable, DisposableBean {
    private final SimpMessagingTemplate messagingTemplate;
    private final BelotRepository belotRepository;
    private final UserRepository userRepository;
    private final BelotPlayerRepository belotPlayerRepository;

    private Queue<User> pendingPlayers;
    private Thread thread;

    @Autowired
    public GameMatchingService(SimpMessagingTemplate messagingTemplate,
                               BelotRepository belotRepository,
                               UserRepository userRepository,
                               BelotPlayerRepository belotPlayerRepository) {
        this.messagingTemplate = messagingTemplate;
        this.belotRepository = belotRepository;
        this.userRepository = userRepository;
        this.belotPlayerRepository = belotPlayerRepository;
        this.pendingPlayers = new ArrayDeque<>();
    }

    /**
     * add a user to the waiting queue with players
     *
     * @param u the user
     */
    public void addUserToQueue(User u) {
        pendingPlayers.add(u);
    }

    //TODO:
    public void removePlayerFromQueue(User u) {
        ArrayDeque<User> players = new ArrayDeque<>();
        while (!pendingPlayers.isEmpty()) {
            User usr = pendingPlayers.poll();
            if (usr.equals(u))
                break;
            players.push(usr);
        }

        while (!players.isEmpty()) {
            pendingPlayers.add(players.pop());
        }
    }

    /**
     * Wait until the queue with players has at least the minimum required players for a game to start
     * Creates a game and sends request to clients to confirm they are ready -> ( {@link Header#READY_CHECK} )
     * TODO: match players by their rank or some other criteria
     * TODO: some try catch block, because when an exception is thrown here
     * the thread stops running
     */
    @Override
    public void run() {
        try {
            //This is daemon thread so its ok for an infinite loop
            //noinspection InfiniteLoopStatement
            while (true) {
                if (pendingPlayers.size() >= NEEDED_PLAYERS_TO_START_A_GAME) {
                    User[] players = new User[NEEDED_PLAYERS_TO_START_A_GAME];
                    for (int i = 0; i < NEEDED_PLAYERS_TO_START_A_GAME; i++) {
                        players[i] = pendingPlayers.poll();
                    }
                    createGame(players);
                }
            }
        } catch (Exception e) {
            log.error("GameMatchingService threw an exception", e);
        }
    }

    @Override
    public void destroy() throws Exception {
        thread.interrupt();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.thread = new Thread(this);

        thread.setDaemon(true);
        thread.start();
    }

    private void createGame(User[] players) {
        cleanAnyPreviousGameData(players);
        Belot belot = new Belot();
        belot.setGameType(GameType.RANKED_GAME);
        belot.setBelotPlayers(
                Arrays.stream(players)
                        .map(p -> new BelotPlayer(p, belot))
                        .collect(Collectors.toSet()));

        final Set<BelotPlayer> playersInGame = belot.getBelotPlayers();
        final int size = playersInGame.size();
        if (size < 4) {
            playersInGame.addAll(Arrays.asList(createDummyPlayers(4 - NEEDED_PLAYERS_TO_START_A_GAME, belot)));
        }

        belot.readyToStart();
        belotRepository.saveAndFlush(belot);

        SocketServerMessage serverMessage = new SocketServerMessage();
        serverMessage.setHeader(Header.READY_CHECK);
        serverMessage.setMessage(belot.getId());

        //Send the message to every player of the game individually
        for (BelotPlayer player : playersInGame) {
            messagingTemplate.convertAndSendToUser(player.getUser().getUsername(),
                    AbstractGameService.CLIENT_SUBSCRIBE_URL,
                    serverMessage);
        }
    }

    private void cleanAnyPreviousGameData(User[] players) {
        for (int i = 0; i < players.length; i++) {
            BelotPlayer belotPlayer = belotPlayerRepository.findByUser(players[i]);
            if (belotPlayer != null)
                belotPlayerRepository.deleteById(belotPlayer.getId());
        }

    }

    /**
     * This is for testing purposes only, and should be removed
     * (or replaced with AI)
     *
     * @param amount the amount of dummy players
     * @param belot  the belot to which the players belong
     * @return the belot players
     */
    private BelotPlayer[] createDummyPlayers(int amount, Belot belot) {

        BelotPlayer[] players = new BelotPlayer[amount];
        for (int i = 0; i < players.length; i++) {
            User u = userRepository.findByUsername(DummyUsersGenerator.names[i]);

            players[i] = new BelotPlayer();
            players[i].setBelot(belot);
            players[i].setUser(u);
        }

        return players;
    }
}
