package com.isoft.belot.services.game.impl;

import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Aleksandar Todorov
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
@Lazy
public class PointsService extends AbstractGameService
{
    public PointsService(SocketClientMessage message, String belotId)
    {
        super(message, belotId);
    }

    @Override
    public void processMessage()
    {

        //TODO who won the Dealing?
        // score points
        // check if some(or both) team has more than 151
        // check if last Dealing is 'valat' and if yes next Dealing
        // congratulate the winners and redirect to choose new game

    }


    @Override
    @Autowired
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    @Autowired
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

    @Override
    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }
}
