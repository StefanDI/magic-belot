package com.isoft.belot.services.game.impl;

import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.services.game.GameMatchingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Stefan Ivanov
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
@Lazy
public class CancelSearchService extends AbstractGameService
{

    private GameMatchingService gameMatchingService;

    public CancelSearchService(SocketClientMessage message,
                               String belotId)
    {
        super(message, belotId);
    }

    @Override
    public void processMessage()
    {
        gameMatchingService.removePlayerFromQueue(clientMessage.getSender());
    }

    @Autowired
    public void setGameMatchingService(GameMatchingService gameMatchingService)
    {
        this.gameMatchingService = gameMatchingService;
    }

    @Override
    public void setBelotRepository(BelotRepository belotRepository)
    {
        super.belotRepository = belotRepository;
    }

    @Override
    public void setPlayerRepository(BelotPlayerRepository playerRepository)
    {
        super.playerRepository = playerRepository;
    }

    @Override
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate)
    {
        super.messagingTemplate = messagingTemplate;
    }
}
