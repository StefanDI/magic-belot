package com.isoft.belot.services;

import com.isoft.belot.domain.entities.Belot;
import com.isoft.belot.repository.BelotRepository;
import com.isoft.belot.services.interfaces.BelotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
@Service
public class BelotServiceImpl implements BelotService
{
    private final BelotRepository belotRepository;

    @Autowired
    public BelotServiceImpl(BelotRepository belotRepository)
    {
        this.belotRepository = belotRepository;
    }


    @Override
    public void deleteAll()
    {
        belotRepository.deleteAll();
    }

    @Override
    public void insert(Belot b)
    {
        belotRepository.saveAndFlush(b);
    }

    @Override
    public void deleteById(String belotId)
    {
        belotRepository.deleteById(belotId);
    }

    @Override
    public Belot getById(String belotId)
    {
        return belotRepository.findById(belotId).orElse(null);
    }

    @Override
    public boolean update(Belot b)
    {
        if (b.getId() != null && !b.getId().isEmpty())
        {
            belotRepository.save(b);
            return true;
        } else
            return false;
    }

    @Override
    public List<Belot> findAll()
    {
        return belotRepository.findAll();
    }
}
