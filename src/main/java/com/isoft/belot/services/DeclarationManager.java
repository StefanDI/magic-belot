package com.isoft.belot.services;

import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.Card;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.CardValue;
import com.isoft.belot.domain.entities.enums.Declaration;
import com.isoft.belot.game.InMemoryBelots;
import com.isoft.belot.services.interfaces.DealerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Aleksandar Todorov
 */
@Service
public class DeclarationManager
{
    private DealerService dealerService;

    public void checkForDeclarations(String belotId, BelotPlayer currentBelotPlayer, AnnounceType roundAnnounceType)
    {
        if (currentBelotPlayer.getCards().size() == 8 && !roundAnnounceType.equals(AnnounceType.NO_TRUMPS))
        {
            Set<Card> cards = currentBelotPlayer.getCards();
            List<Declaration> squareDeclarations = findSquareDeclarations(cards);
            List<Declaration> chainDeclarations = new ArrayList<>();

            //There are 3 options -> 0 squares, 1 square, 2 squares
            if (squareDeclarations.isEmpty())
            {
                chainDeclarations = findChainDeclarations(cards);
            } else if (squareDeclarations.size() == 1)
            {
                cards = cards
                        .stream()
                        .filter(card -> !card.getCardValue().equals(squareDeclarations.get(0).getCardValue()))
                        .collect(Collectors.toSet());
                cards = dealerService.orderCards(cards);
                chainDeclarations = findChainDeclarations(cards);
            }

            squareDeclarations.addAll(chainDeclarations);

            InMemoryBelots.teamsDeclarations.get(belotId).put(currentBelotPlayer.getTeam(), squareDeclarations);
        }
    }

    public void checkForBelotDeclaration(String belotId, BelotPlayer belotPlayer, Card card, AnnounceType roundAnnounceType)
    {
        if (!roundAnnounceType.equals(AnnounceType.NO_TRUMPS) && (
                card.getCardValue().equals(CardValue.KING) ||
                        card.getCardValue().equals(CardValue.QUEEN)))
        {
            if (roundAnnounceType.equals(AnnounceType.ALL_TRUMPS) ||
                    roundAnnounceType.name().equals(card.getSuit().name()))
                checkKQInYourCards(belotId, belotPlayer, card);
        }
    }

    private void checkKQInYourCards(String belotId, BelotPlayer belotPlayer, Card card)
    {
        if (card.getCardValue().equals(CardValue.KING) &&
                belotPlayer.getCards().contains(new Card(card.getSuit(), CardValue.QUEEN)) ||
                card.getCardValue().equals(CardValue.QUEEN) &&
                        belotPlayer.getCards().contains(new Card(card.getSuit(), CardValue.KING)))
            InMemoryBelots.teamsDeclarations.get(belotId).get(belotPlayer.getTeam()).add(Declaration.BELOT);
    }

    //we should not play NO_TRUMPS
    public List<Declaration> findSquareDeclarations(Set<Card> cards)
    {
        List<Declaration> squareDeclarations = new ArrayList<>();

        //check from NINE to ACE
        for (int i = 2; i < 8; i++)
        {
            int finalI = i;
            if (cards
                    .stream()
                    .filter(card -> card.getCardValue().ordinal() == finalI)
                    .collect(Collectors.toSet()).size() == 4)
                addDeclaration(squareDeclarations, Declaration.SQUARE, CardValue.values()[finalI]);
        }

        return squareDeclarations;
    }

    //we should not play NO_TRUMPS
    public List<Declaration> findChainDeclarations(Set<Card> cards)
    {
        List<Declaration> chainDeclarations = new ArrayList<>();

        int consecutiveCards = 1;
        Card prevCard = null;

        for (Card card : cards)
        {
            if (prevCard == null)
            {
                prevCard = card;
                continue;
            }

            if (prevCard.getSuit().equals(card.getSuit()) &&
                    prevCard.getCardValue().ordinal() + 1 == card.getCardValue().ordinal())
                consecutiveCards++;
            else
            {
                if (consecutiveCards >= 3)
                {
                    switch (consecutiveCards)
                    {
                        case 3:
                            addDeclaration(chainDeclarations, Declaration.CHAIN_OF_3, prevCard.getCardValue());
                            break;
                        case 4:
                            addDeclaration(chainDeclarations, Declaration.CHAIN_OF_4, prevCard.getCardValue());
                            break;
                        case 8:
                            addDeclaration(chainDeclarations, Declaration.CHAIN_OF_5_PLUS, prevCard.getCardValue());
                            addDeclaration(chainDeclarations, Declaration.CHAIN_OF_3, prevCard.getCardValue());
                            return chainDeclarations;
                        default:
                            addDeclaration(chainDeclarations, Declaration.CHAIN_OF_5_PLUS, prevCard.getCardValue());
                            break;
                    }
                }
                consecutiveCards = 1;
            }

            prevCard = card;
        }

        return chainDeclarations;
    }

    //set strongest CardValue to the Declaration and add it to list.
    private void addDeclaration(List<Declaration> declarations, Declaration declaration, CardValue cardValue)
    {
        declaration.setCardValue(cardValue);
        declarations.add(declaration);
    }

    @Autowired
    public void setDealerService(DealerServiceImpl dealerServiceImpl)
    {
        this.dealerService = dealerServiceImpl;
    }
}
