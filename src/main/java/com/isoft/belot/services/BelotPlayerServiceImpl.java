package com.isoft.belot.services;

import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.UserRepository;
import com.isoft.belot.services.interfaces.BelotPlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Stefan Ivanov
 */
@Service
public class BelotPlayerServiceImpl implements BelotPlayerService
{
    private final BelotPlayerRepository playerRepository;
    private final UserRepository userRepository;

    @Autowired
    public BelotPlayerServiceImpl(BelotPlayerRepository playerRepository, UserRepository userRepository)
    {
        this.playerRepository = playerRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void update(BelotPlayer player)
    {
        playerRepository.saveAndFlush(player);
    }

    @Override
    public void updateAll(Iterable<BelotPlayer> players)
    {
        playerRepository.saveAll(players);
    }

    @Override
    public void remove(BelotPlayer player)
    {
        playerRepository.delete(player);
    }

    @Override
    public void removeAll(Iterable<BelotPlayer> players)
    {
        playerRepository.deleteAll(players);
    }

    @Override
    public void removeByUsername(String username)
    {
        User user = userRepository.findByUsername(username);

        BelotPlayer toBeDeleted =
                playerRepository.findByUser(user);

        if(toBeDeleted == null)
            return;

        toBeDeleted.setMagicCards(null);

        playerRepository.saveAndFlush(toBeDeleted);

        playerRepository.deleteById(toBeDeleted.getId());
    }
}
