package com.isoft.belot.services.interfaces;

import com.isoft.belot.domain.entities.Belot;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
public interface BelotService
{
    /**
     * For debugging only, should be removed in release
     */
    void deleteAll();

    void insert(Belot b);

    void deleteById(String belotId);

    Belot getById(String belotId);

    /**
     * Update the belot
     * @param b the belot with proper id set
     * @return true if successfully, false if no id is set (therefore cannot update)
     */
    boolean update(Belot b);

    List<Belot> findAll();
}
