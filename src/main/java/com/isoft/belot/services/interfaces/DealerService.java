package com.isoft.belot.services.interfaces;

import com.isoft.belot.domain.entities.*;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.Declaration;

import java.util.List;
import java.util.Set;

/**
 * @author Aleksandar Todorov
 */
public interface DealerService
{
    Deck dealCards(Belot belot, Deck deck, int numberToDraw);

    /**
     * Deal the rest of the cards when Announce Phase ends.
     * Every player should have 8 cards, so he is dealt 8 - current Hand size
     */
    void dealTheRestOfCards(Belot belot);

    /**
     * Order cards of this belotPlayer
     *
     * @param belotPlayer which cards will be ordered
     */
    void orderCards(BelotPlayer belotPlayer);

    Set<Card> orderCards(Set<Card> cards);

    void clearCards(Belot belot);

    /**
     * Calculate the points from the cards which are won by the first team depending on the current announceType.
     *
     * @param firstTeamWonCards Set<Card> with all the cards won by first Team
     * @param announceType      the current announceType
     * @return int value of all points from the cards from the first Team
     */
    int calculateFirstTeamCardsPoints(Set<Card> firstTeamWonCards, AnnounceType announceType);

    /**
     * @param announceType      of the game
     * @param validDeclarations only valid Declarations
     * @return whole points of the game plus all valid Declarations
     */
    int calculateDealingPoints(AnnounceType announceType, List<Declaration> validDeclarations);

    /**
     * @param firstTeamCardsPoints int value of all the points from the cards from the first team
     * @param dealingPoints        int value of all the points plus Declarations in the dealing
     * @param multiplier           int value[4,2,0] depending on RE_DOUBLE, DOUBLE or non is announced
     * @param isFirstTeamAnnounce  whether first team announced the AnnounceType
     * @return
     */
    void determineTeamPoints(Belot belot, int firstTeamCardsPoints, int dealingPoints, int multiplier, boolean isFirstTeamAnnounce);

    /**
     * Current AnnounceType is the 4th,5th or 6th from the last of the List<Announce>,
     * because the last three AnnounceTypes are PASS.
     * YOU SHOULD ALWAYS call {@link Dealing#twoConsecutivePass()} before you call this method
     * or Exception will be thrown
     *
     * @return 4th, 5th or 6th AnnounceType from the last of the List
     */
    AnnounceType determineRoundAnnounceType(Dealing dealing);

    /**
     * YOU SHOULD ALWAYS call {@link Dealing#twoConsecutivePass()} before you call this method
     * or Exception will be thrown
     *
     * @return 4, 2 or 1
     */
    int determinePointsMultiplier(Dealing dealing);

    /**
     * the list of Cards should be always be 4, although the method works with more or less Cards.
     * IMPORTANT - first card in the list should be the card of the first player which is first to play.
     * tested.
     */
    Card whichCardWins(Set<Card> cards, AnnounceType announceType);

    /**
     * get Strongest Trump Card from the same Suit as First Card
     */
    Card getStrongestTrumpCard(Set<Card> cards);

    /**
     * returns the strongest TrumpPoint Card from the AnnounceType
     */
    Card getStrongestTrumpCard(Set<Card> cards, AnnounceType announceType);
}
