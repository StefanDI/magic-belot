package com.isoft.belot.services.interfaces;

import com.isoft.belot.domain.entities.User;
import com.isoft.belot.domain.models.binding.UserBinding;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService
{
    void add(UserBinding user);

    void remove(UserBinding user);

    boolean usernameExists(String username);

    boolean emailExists(String email);

    User findById(String id);

    void update(User u);

    User findByUsername(String username);

    List<User> findAll();
}
