package com.isoft.belot.services.interfaces;

import com.isoft.belot.controllers.BelotController;
import com.isoft.belot.services.game.impl.AbstractGameService;

/**
 * API for processing web socket messages
 * currently used only for belot, but if there is another game,
 * or another way of communication this interface should be used
 * Classes that implement this, can have any kind of message in them,
 * the controllers will have to pass the desired message implementation
 * to this service's implementation and call {@link #processMessage()}
 *
 * @see AbstractGameService for example implementation and
 * @see BelotController for example usage
 */
public interface MessageHandlingService
{
    /**
     * Processes the message
     */
    void processMessage();
}
