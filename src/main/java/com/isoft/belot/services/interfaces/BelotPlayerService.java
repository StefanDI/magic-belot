package com.isoft.belot.services.interfaces;

import com.isoft.belot.domain.entities.BelotPlayer;

public interface BelotPlayerService
{
    void update(BelotPlayer player);

    void updateAll(Iterable<BelotPlayer> players);

    void remove(BelotPlayer player);

    void removeAll(Iterable<BelotPlayer> players);

    void removeByUsername(String username);
}
