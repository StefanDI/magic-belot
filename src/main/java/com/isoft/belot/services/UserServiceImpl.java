package com.isoft.belot.services;

import com.isoft.belot.domain.entities.Role;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.domain.models.binding.UserBinding;
import com.isoft.belot.repository.RoleRepository;
import com.isoft.belot.repository.UserRepository;
import com.isoft.belot.services.interfaces.UserService;
import com.isoft.belot.util.interfaces.ValidatorUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService
{
    private static final String ROLE_ROOT = "ROLE_ROOT";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String ROLE_MODERATOR = "ROLE_MODERATOR";
    private static final String ROLE_USER = "ROLE_USER";
    private static final String USERNAME_NOT_FOUND = "Username not found!";

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleRepository roleRepository;
    private final ValidatorUtil validatorUtil;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           ModelMapper modelMapper,
                           BCryptPasswordEncoder bCryptPasswordEncoder,
                           RoleRepository roleRepository,
                           ValidatorUtil validatorUtil)
    {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleRepository = roleRepository;
        this.validatorUtil = validatorUtil;
    }

    @Override
    public void add(UserBinding userBinding)
    {
        User user = this.modelMapper.map(userBinding, User.class);
        if (!userBinding.getPassword().equals(userBinding.getConfirmPassword())
                || !validatorUtil.isValid(user))
        {
            return;
        }

        user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));

        this.insertRoles();
        addRole(user, ROLE_USER);

        if (this.userRepository.count() == 0)
        {
            addUserModeratorAdminRoles(user);
            addRole(user, ROLE_ROOT);
        } else
        {
            addRole(user, ROLE_USER);
        }

        try
        {
            this.userRepository.save(user);
        } catch (Exception e)
        {
            log.error("Couldn't save user in users table in DB", e);
        }

    }

    private void addRole(User user, String roleUser)
    {
        user.getAuthorities().add(this.roleRepository.findByAuthority(roleUser));
    }

    private void addUserModeratorAdminRoles(User user)
    {
        addRole(user, ROLE_USER);
        addRole(user, ROLE_MODERATOR);
        addRole(user, ROLE_ADMIN);
    }

    private void insertRoles()
    {
        if (this.roleRepository.count() == 0)
        {
            Role root = new Role();
            root.setAuthority(ROLE_ROOT);
            Role admin = new Role();
            admin.setAuthority(ROLE_ADMIN);
            Role moderator = new Role();
            moderator.setAuthority(ROLE_MODERATOR);
            Role user = new Role();
            user.setAuthority(ROLE_USER);

            this.roleRepository.save(root);
            this.roleRepository.save(admin);
            this.roleRepository.save(moderator);
            this.roleRepository.save(user);
        }
    }

    @Override
    public void remove(UserBinding user)
    {
        if (user.getId() != null)
            this.userRepository.deleteById(user.getId());
    }

    @Override
    public boolean usernameExists(String username)
    {
        return this.userRepository.findByUsername(username) != null;
    }

    @Override
    public boolean emailExists(String email)
    {
        return this.userRepository.findByEmail(email) != null;
    }

    @Override
    public User findById(String id)
    {
        return this.userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("No user found with id " + id));
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User user;
        if (username.contains("@"))
            user = this.userRepository.findByEmail(username);
        else
            user = this.userRepository.findByUsername(username);

        if (user == null)
        {
            throw new UsernameNotFoundException(USERNAME_NOT_FOUND);
        }
        return user;
    }

    @Override
    public void update(User u)
    {
        this.userRepository.save(u);
    }

    @Override
    public User findByUsername(String username)
    {
        return this.userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }
}
