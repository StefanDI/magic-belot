package com.isoft.belot.services;

import com.google.common.collect.Iterables;
import com.isoft.belot.domain.entities.*;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.Declaration;
import com.isoft.belot.services.interfaces.DealerService;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 * TODO: Deal like normal belot ( 3 - 2 - 3 )
 *  and shuffle the deck only once
 * @author Aleksandar Todorov
 */
@Service
public final class DealerServiceImpl implements DealerService
{
    @Override
    public Deck dealCards(Belot belot, Deck deck, int numberToDraw)
    {
        for (BelotPlayer belotPlayer : belot.getBelotPlayers())
        {
            for (int i = 0; i < numberToDraw; i++)
            {
                belotPlayer.addCard(deck.drawCard());
            }
        }
        return deck;
    }

    @Override
    public void dealTheRestOfCards(Belot belot)
    {
        Deck deck = Deck.getDeck(belot);
        for (BelotPlayer belotPlayer : belot.getBelotPlayers())
        {
            int numberOfCardsInPlayer = belotPlayer.getCards().size();
            for (int i = 0; i < 8 - numberOfCardsInPlayer; i++)
            {
                belotPlayer.addCard(deck.drawCard());
            }
            this.orderCards(belotPlayer);
        }
    }

    @Override
    public void orderCards(BelotPlayer belotPlayer)
    {
        belotPlayer.setCards(belotPlayer.getCards().stream()
                .sorted((o1, o2) ->
                {
                    if (o1.getSuit().ordinal() - o2.getSuit().ordinal() == 0)
                        return o1.getCardValue().ordinal() - o2.getCardValue().ordinal();
                    else return o1.getSuit().ordinal() - o2.getSuit().ordinal();
                }).collect(Collectors.toCollection(LinkedHashSet::new)));
    }

    @Override
    public Set<Card> orderCards(Set<Card> cards)
    {
        cards = cards.stream()
                .sorted((o1, o2) ->
                {
                    if (o1.getSuit().ordinal() - o2.getSuit().ordinal() == 0)
                        return o1.getCardValue().ordinal() - o2.getCardValue().ordinal();
                    else return o1.getSuit().ordinal() - o2.getSuit().ordinal();
                }).collect(Collectors.toCollection(LinkedHashSet::new));

        return cards;
    }

    @Override
    public void clearCards(Belot belot)
    {
        for (BelotPlayer player : belot.getBelotPlayers())
        {
            player.getCards().clear();
        }
    }

    @Override
    public int calculateFirstTeamCardsPoints(final Set<Card> firstTeamWonCards, AnnounceType announceType)
    {
        int firstTeamCardsPoints = 0;

        switch (announceType)
        {
            case ALL_TRUMPS:
                firstTeamCardsPoints = firstTeamWonCards.stream().map(x -> x.getCardValue().getTrumpPoints()).reduce(0, Integer::sum);
                break;
            case NO_TRUMPS: // the points are multiply by 2
                firstTeamCardsPoints = firstTeamWonCards.stream().map(x -> x.getCardValue().getNoTrumpPoints()).reduce(0, Integer::sum) * 2;
                break;
            default:
                for (Card card : firstTeamWonCards)
                {
                    if (card.getSuit().equals(announceType))
                        firstTeamCardsPoints += card.getCardValue().getTrumpPoints();
                    else firstTeamCardsPoints += card.getCardValue().getNoTrumpPoints();
                }
        }
        return firstTeamCardsPoints;
    }

    @Override
    public int calculateDealingPoints(AnnounceType announceType, List<Declaration> validDeclarations)
    {
        switch (announceType)
        {
            case ALL_TRUMPS:
                return 258 + validDeclarations.stream().map(Declaration::getPoints).reduce(0, Integer::sum);
            case NO_TRUMPS:
                return 260 + validDeclarations.stream().map(Declaration::getPoints).reduce(0, Integer::sum);
            default:
                return 162 + validDeclarations.stream().map(Declaration::getPoints).reduce(0, Integer::sum);
        }
    }

    @Override
    public void determineTeamPoints(Belot belot, int firstTeamCardsPoints, int dealingPoints, int multiplier, boolean isFirstTeamAnnounce)
    {
        switch (multiplier)
        {
            case 1:
                //if we announced and have less than the half points we are "in"
                if (isFirstTeamAnnounce && firstTeamCardsPoints < dealingPoints / 2)
                    belot.getTeamsPoints().put(1, dealingPoints / 10);
                    //if they announced and have less than the half points they are "in"
                else if (!isFirstTeamAnnounce && firstTeamCardsPoints > dealingPoints / 2)
                    belot.getTeamsPoints().put(0, dealingPoints / 10);
                    //otherwise every team score their points
                else
                {
                    belot.getTeamsPoints().put(0, firstTeamCardsPoints / 10);
                    belot.getTeamsPoints().put(1, (dealingPoints - firstTeamCardsPoints) / 10);
                }
                break;
            default:
                //if we have less than the half points we are "in" no matter which team announced the dealing
                if (firstTeamCardsPoints < dealingPoints / 2)
                    belot.getTeamsPoints().put(1, (dealingPoints / 10) * multiplier);
                else
                    belot.getTeamsPoints().put(0, (dealingPoints / 10) * multiplier);
        }
    }

    @Override
    public AnnounceType determineRoundAnnounceType(Dealing dealing)
    {
        final int size = dealing.getAnnounces().size();
        switch (dealing.getSortedAnnounces().toArray(new Announce[0])[size - 4].getAnnounceType().name())
        {
            case "RE_DOUBLE":
                return dealing.getSortedAnnounces().toArray(new Announce[0])[size - 6].getAnnounceType();
            case "DOUBLE":
                return dealing.getSortedAnnounces().toArray(new Announce[0])[size - 5].getAnnounceType();
            default:
                return dealing.getSortedAnnounces().toArray(new Announce[0])[size - 4].getAnnounceType();
        }
    }

    @Override
    public int determinePointsMultiplier(Dealing dealing)
    {
        AnnounceType announceType = dealing.getSortedAnnounces().toArray(new Announce[0])[dealing.getAnnounces().size() - 4].getAnnounceType();
        if (announceType.equals(AnnounceType.RE_DOUBLE)) return 4;
        if (announceType.equals(AnnounceType.DOUBLE)) return 2;
        return 1;
    }

    @Override
    public Card whichCardWins(Set<Card> cards, AnnounceType announceType)
    {
        switch (announceType)
        {
            case ALL_TRUMPS:
                return getStrongestTrumpCard(cards);
            case NO_TRUMPS:
                return getStrongestNoTrumpCard(cards);
            default:
                //whether there is Cards from the AnnounceType
                if (cards.stream().anyMatch(card -> announceType.name().equals(card.getSuit().name())))
                    return getStrongestTrumpCard(cards, announceType);

                return getStrongestNoTrumpCard(cards);
        }
    }

    @Override
    public Card getStrongestTrumpCard(Set<Card> cards, AnnounceType announceType)
    {
        return cards.stream()
                .filter(card -> announceType.name().equals(card.getSuit().name()))
                .max(comparing(card -> card.getCardValue().getTrumpPoints())).get();
    }

    @Override
    public Card getStrongestTrumpCard(Set<Card> cards)
    {
        return cards.stream()
                .filter(card -> card.getSuit().equals(Iterables.getFirst(cards, null).getSuit()))
                .max(comparing(card -> card.getCardValue().getTrumpPoints())).get();
    }

    //returns the strongest NoTrumpPoint Card from the Suit of the first Card
    private Card getStrongestNoTrumpCard(Set<Card> cards)
    {
        return cards.stream()
                .filter(card -> card.getSuit().equals(Iterables.getFirst(cards, null).getSuit()))
                .max(comparing(card -> card.getCardValue().getNoTrumpPoints())).get();
    }
}
