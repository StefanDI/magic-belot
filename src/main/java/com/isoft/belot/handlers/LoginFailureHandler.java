package com.isoft.belot.handlers;

import com.isoft.belot.util.JsonParser;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Stefan Ivanov
 * <p>
 * Used by spring to handle error when trying to log in
 * Adds the error message header and sets status to 401 (unauthorized request),
 * needed for front end validation and messaging
 */
@Component
public class LoginFailureHandler implements AuthenticationFailureHandler
{
    private final JsonParser parser;

    public LoginFailureHandler(JsonParser parser) {
        this.parser = parser;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException
    {
        response.getWriter().println(parser.toJson(exception.getMessage()));
        response.setStatus(401);
    }
}
