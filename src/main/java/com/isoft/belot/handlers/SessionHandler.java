package com.isoft.belot.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SessionHandler implements SessionRegistry {
    private final List<Object> principals;
    private final Map<String, SessionInformation> sessions;

    public SessionHandler() {
        this.sessions = new HashMap<>();
        this.principals = new ArrayList<>();
    }


    @Override
    public List<Object> getAllPrincipals() {
        return this.principals;
    }

    @Override
    public List<SessionInformation> getAllSessions(Object o, boolean b) {
        return new ArrayList<SessionInformation>(this.sessions.values());
    }

    @Override
    public SessionInformation getSessionInformation(String s) {
        return this.sessions.get(s);
    }

    @Override
    public void refreshLastRequest(String s) {
        System.out.println(s);
    }

    @Override
    public void registerNewSession(String s, Object o) {
        SessionInformation session = new SessionInformation(o, s, new Date());
        this.sessions.put(s, session);
        System.out.println(s);
        System.out.println(o);
    }

    @Override
    public void removeSessionInformation(String s) {
        this.sessions.remove(s);

        System.out.println(s);
    }
}
