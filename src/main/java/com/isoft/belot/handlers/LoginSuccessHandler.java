package com.isoft.belot.handlers;

import com.isoft.belot.domain.entities.Role;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.util.JsonParser;
import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

/**
 * Appends user data to the response
 *
 * @author Stefan Ivanov
 */
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    private final JsonParser jsonParser;

    public LoginSuccessHandler(JsonParser jsonParser) {
        this.jsonParser = jsonParser;
    }


    @SneakyThrows
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        //TODO: Update UserViewModel to show relevant data
        // for example we dont need to send password
        User loggedInUser = (User) authentication.getPrincipal();
        if (session != null) {
            loggedInUser.setSessionId(session.getId());
            session.setAttribute("user", loggedInUser.getId());
        }
        response.getWriter().append(jsonParser.toJson(loggedInUser));
    }
}
