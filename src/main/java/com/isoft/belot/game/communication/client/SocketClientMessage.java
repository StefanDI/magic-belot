package com.isoft.belot.game.communication.client;

import com.isoft.belot.domain.entities.User;
import com.isoft.belot.game.communication.client.enums.ClientMessageType;
import lombok.NoArgsConstructor;

/**
 * @author Stefan Ivanov
 */
@NoArgsConstructor
public class SocketClientMessage {

    private User sender;

    private String payload;

    private ClientMessageType clientMessageType;

    public SocketClientMessage(String payload, ClientMessageType clientMessageType) {
        this.payload = payload;
        this.clientMessageType = clientMessageType;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public ClientMessageType getType() {
        return clientMessageType;
    }

    public void setType(ClientMessageType clientMessageType) {
        this.clientMessageType = clientMessageType;
    }
}
