package com.isoft.belot.game.communication.client.enums;

public enum ClientMessageType
{
    CREATE_PRIVATE_GAME,
    JOIN_PRIVATE_GAME,
    SEARCH_RANKED,
    CANCEL_SEARCH,
    READY,
    ANNOUNCE,
    PLAY_CARD,
    LEAVE_GAME,
    CHECK_IF_IN_GAME
}
