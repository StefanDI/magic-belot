package com.isoft.belot.game.communication.server.enums;

public enum Header
{
    ERROR,
    SUCCESS,
    PLAYER_JOINED,
    PLAYER_LEFT,
    READY_CHECK,
    SUBSCRIBE_TO,
    ANNOUNCE,
    PLAYERS_INFO,
    PLAYER_ACTION,
    CARDS,
    YOUR_TURN;
}
