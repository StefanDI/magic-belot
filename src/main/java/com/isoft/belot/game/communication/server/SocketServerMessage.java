package com.isoft.belot.game.communication.server;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isoft.belot.game.communication.server.enums.Header;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Stefan Ivanov
 */
@NoArgsConstructor
@Getter
@Setter
public class SocketServerMessage {

    private String message;

    private Header header;

    @JsonIgnore
    private String sendToSpecific;

    public SocketServerMessage(String message) {
        this.message = message;
    }
}
