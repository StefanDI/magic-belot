package com.isoft.belot.game;

import com.isoft.belot.domain.entities.Belot;
import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.Card;
import com.isoft.belot.domain.entities.enums.Declaration;
import com.isoft.belot.domain.entities.enums.Team;
import com.isoft.belot.services.game.announce.AnnounceStateContext;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * This is the class holding all in memory data for optimization purposes.
 *
 * @author Stefan Ivanov
 * @author Aleksandar Todorov
 */
@NoArgsConstructor
public final class InMemoryBelots
{
    /**
     * String --> belotId
     */
    public static final ConcurrentMap<String, Belot> allOpenBelots = new ConcurrentHashMap<>();

    /**
     * String --> belotId
     * Boolean[] --> must have 2 elements ---> first : canDouble , second : canRedouble
     */
    public static final ConcurrentMap<String, Map<BelotPlayer, Boolean[]>> doublesOptions = new ConcurrentHashMap<>();

    /**
     * String --> belotId
     * belotPlayer --> the belotPlayer who plays first every Dealing
     */
    public static final ConcurrentMap<String, BelotPlayer> belotPlayerPlayFirst = new ConcurrentHashMap<>();

    /**
     * String --> belotId
     * Set<Card> - 4 cards from this round. The first card determent the suit that you NEED to play.
     */
    public static final ConcurrentMap<String, Set<Card>> roundCards = new ConcurrentHashMap<>();

    /**
     * String --> belotId
     * all remaining cards of each belotPlayer
     */
    public static final ConcurrentMap<String, HashMap<BelotPlayer, Set<Card>>> playersCards = new ConcurrentHashMap<>();

    /**
     * String --> belotId
     * every Card is played from a belotPlayer
     */
    public static final ConcurrentMap<String, HashMap<Card, BelotPlayer>> playedCards = new ConcurrentHashMap<>();

    /**
     * String --> belotId
     * Set<Card> - winning cards which should be calculated at the end.
     * We don't need the second team won cards.
     */
    public static final ConcurrentMap<String, Set<Card>> firstTeamCardsWon = new ConcurrentHashMap<>();

    /**
     * String --> belotId
     * Team --> from which team is the player.
     * List<Declaration> all declaration from a team
     */
    public static final ConcurrentMap<String, Map<Team, List<Declaration>>> teamsDeclarations = new ConcurrentHashMap<>();

    /**
     * Holds the announce state machine for each belot
     */
    public static final ConcurrentMap<String, AnnounceStateContext> openBelotsAnnounces = new ConcurrentHashMap<>();
}
