package com.isoft.belot.repository;

import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BelotPlayerRepository extends JpaRepository<BelotPlayer, String>
{
    BelotPlayer findByUser(User u);

    @Transactional
    @Modifying
    @Query("DELETE FROM BelotPlayer WHERE id = ?1")
    void deleteById(String id);
}
