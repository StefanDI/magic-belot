package com.isoft.belot.repository;

import com.isoft.belot.domain.entities.Belot;
import com.isoft.belot.domain.entities.Dealing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DealingRepository extends JpaRepository<Dealing, String>
{
    Dealing getFirstByBelotOrderByCreatedDesc(Belot belot);
}