package com.isoft.belot.repository;

import com.isoft.belot.domain.entities.Belot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BelotRepository extends JpaRepository<Belot, String>
{
    Belot getById(String id);
}