package com.isoft.belot.repository;

import com.isoft.belot.domain.entities.Announce;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Aleksandar Todorov
 */
@Repository
public interface AnnounceRepository extends JpaRepository<Announce, String>
{
}
