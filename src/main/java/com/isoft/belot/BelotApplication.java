package com.isoft.belot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelotApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(BelotApplication.class, args);
    }
}

