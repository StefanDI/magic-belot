package com.isoft.belot.domain.models.binding;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Stefan Ivanov
 */
@NoArgsConstructor
@Getter
@Setter
public class UserBinding extends BaseBinding {

    private String email;

    private String username;

    private String password;

    private String confirmPassword;
}
