package com.isoft.belot.domain.models.view.belot;

import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.MagicCard;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Stefan Ivanov
 */
@Getter
@Setter
public class AnnounceViewModel
{
    private Map<AnnounceType, Boolean> availableAnnounces;
    private Map<MagicCard, Boolean> availableMagicCards;

    public AnnounceViewModel()
    {
        availableAnnounces = new LinkedHashMap<>();
        availableMagicCards = new LinkedHashMap<>();
    }
}
