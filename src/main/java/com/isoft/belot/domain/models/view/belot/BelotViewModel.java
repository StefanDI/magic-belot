package com.isoft.belot.domain.models.view.belot;

import com.isoft.belot.domain.entities.BelotPlayer;

/**
 * @author Stefan Ivanov
 */
public class BelotViewModel
{
    private Player partner;
    private Player left;
    private Player right;

    public Player getPartner()
    {
        return partner;
    }

    public void setPartner(BelotPlayer partner)
    {
        this.partner = toPlayer(partner);
    }

    public Player getLeft()
    {
        return left;
    }

    public void setLeft(BelotPlayer left)
    {
        this.left = toPlayer(left);
    }

    public Player getRight()
    {
        return right;
    }

    public void setRight(BelotPlayer right)
    {
        this.right = toPlayer(right);
    }

    private Player toPlayer(BelotPlayer partner)
    {
        Player player = new Player();
        player.cards = partner.getCards().size();
        player.username = partner.getUser().getUsername();
        return player;
    }

    @SuppressWarnings("InnerClassMayBeStatic")
    public class Player{
        private int cards;
        private String username;

        public String getUsername()
        {
            return username;
        }
    }
}
