package com.isoft.belot.domain.models.view.belot;

import lombok.Getter;
import lombok.Setter;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author Stefan Ivanov
 */
@Getter
@Setter
public class PlayerActionViewModel
{
    /**
     * String --> renders FRONT END
     */
    private Map<TurnType, String> turnInfo;
    private String username;

    public PlayerActionViewModel()
    {
        turnInfo = new EnumMap<>(TurnType.class);
    }

    public enum TurnType
    {
        ANNOUNCE, CARD_PLAYED, MAGIC_CARD_PLAYED;
    }
}
