package com.isoft.belot.domain.models.view;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Stefan Ivanov
 */
@Getter
@Setter
public class UserView
{
    private String username;
    private String email;
    private String id;
}
