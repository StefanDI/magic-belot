package com.isoft.belot.domain.entities.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * during the game there are extra points for KQ (Belot), chain of cards(3,4 and 5+)
 * or square(4 of a kind)
 * SQUARE beats CHAIN_OF_5_PLUS
 *
 * @return points[20, 30, 50, 100]
 * Not an Entity.
 */

@Getter
public enum Declaration
{
    BELOT(20),
    CHAIN_OF_3(20), //tierce
    CHAIN_OF_4(50), //quarte
    CHAIN_OF_5_PLUS(100), //quinte
    SQUARE(100); // 4 of a kind

    private int points;

    @Setter
    private CardValue cardValue;

    Declaration(int points)
    {
        this.points = points;
    }
}
