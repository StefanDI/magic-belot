package com.isoft.belot.domain.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Aleksandar Todorov
 */
@Data
@MappedSuperclass
abstract class BaseCreatedEntity extends BaseEntity
{
    @Column(name = "created", nullable = false, updatable = false)
    @NotNull
    private LocalDateTime created;

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof BaseCreatedEntity)) return false;
        BaseCreatedEntity baseCreatedEntity = (BaseCreatedEntity) o;

        return baseCreatedEntity.getCreated().equals(getCreated());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(created);
    }
}
