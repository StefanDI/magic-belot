package com.isoft.belot.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.isoft.belot.domain.entities.enums.MagicCard;
import com.isoft.belot.domain.entities.enums.Team;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * BelotPlayer Entity
 * <p>
 * {@link #user      } @OneToOne relation unique
 * {@link #belot     } @ManyToOne
 * {@link #cards     } @ManyToMany(3rd table) with Entity Card(32 different cards in the DB)
 * {@link #magicCards} @ManyToMany(3rd table) with Enum(NOT a entity) MagicCard[0-1]
 * {@link #team      } @OneToOne with Enum(NOT a entity) Team[0-4]
 *
 * @author Stefan Ivanov
 */
@Setter
@Getter
@Entity
@Table(name = "belot_players")
public class BelotPlayer extends BaseEntity
{
    @OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "belot_id", referencedColumnName = "id", nullable = false, updatable = false)
    @JsonBackReference
    private Belot belot;

    @Transient
    private Set<Card> cards;

    @ElementCollection(targetClass = MagicCard.class, fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "belot_player_id")
    @CollectionTable(name = "belot_players_magic_cards")
    @Column(name = "magic_card_id")
    private Set<MagicCard> magicCards;

    private Team team;

    public BelotPlayer()
    {
        this.cards = new HashSet<>();
        this.magicCards = new LinkedHashSet<>();
        magicCards.add(MagicCard.BLIZZARD_STORM);
        magicCards.add(MagicCard.DESTINY_CREATOR);
        this.team = Team.NOT_IN_GAME;
    }

    public BelotPlayer(User user, Belot belot)
    {
        this.user = user;
        this.belot = belot;
        this.cards = new HashSet<>();
        this.magicCards = new LinkedHashSet<>();
        magicCards.add(MagicCard.BLIZZARD_STORM);
        magicCards.add(MagicCard.DESTINY_CREATOR);
        this.team = Team.NOT_IN_GAME;
    }

    public void addCard(Card card)
    {
        this.cards.add(card);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BelotPlayer that = (BelotPlayer) o;
        return user.equals(that.user);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(user);
    }

    public boolean isFirstTeam()
    {
        return team.equals(Team.TEAM_ONE_FIRST) || team.equals(Team.TEAM_ONE_SECOND);
    }
}
