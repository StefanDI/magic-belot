package com.isoft.belot.domain.entities.enums;

/**
 * The 4 different suits of cards - Clubs ♣ Diamonds ♦ Hearts ♥ Spades ♠
 * Not an Entity.
 */
public enum Suit
{
    CLUBS,
    DIAMONDS,
    HEARTS,
    SPADES
}
