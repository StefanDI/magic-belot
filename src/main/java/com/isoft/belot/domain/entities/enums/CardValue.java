package com.isoft.belot.domain.entities.enums;

/**
 * 8 different card values. With points representations.
 * NINE and JACK have different points when they are from the TRUMP announced(AnnounceType).
 * NINE trump Points are 14.
 * JACK trump Points are 20.
 * Not an Entity.
 */
public enum CardValue
{
    SEVEN(0, 0, 0, 0),
    EIGHT(0, 1, 0, 1),
    NINE(0, 2, 14, 6),
    TEN(10, 6, 10, 4),
    JACK(2, 3, 20, 7),
    QUEEN(3, 4, 3, 2),
    KING(4, 5, 4, 3),
    ACE(11, 7, 11, 5);

    private int noTrumpPoints;
    private int noTrumpStrength;
    private int trumpPoints;
    private int trumpStrength;

    CardValue(int noTrumpPoints, int noTrumpStrength, int trumpPoints, int trumpStrength)
    {
        this.noTrumpPoints = noTrumpPoints;
        this.noTrumpStrength = noTrumpStrength;
        this.trumpPoints = trumpPoints;
        this.trumpStrength = trumpStrength;
    }

    public int getNoTrumpPoints()
    {
        return noTrumpPoints;
    }

    public int getTrumpPoints()
    {
        return trumpPoints;
    }
}
