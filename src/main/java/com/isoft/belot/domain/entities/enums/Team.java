package com.isoft.belot.domain.entities.enums;

/**
 * There are 2 teams from 2 players each. Or the game is not started yet and
 * the default value is NOT_IN_GAME.
 *
 * @return value[0-4]
 * Not an Entity.
 */
public enum Team
{
    NOT_IN_GAME(0),
    TEAM_ONE_FIRST(1),
    TEAM_TWO_FIRST(2),
    TEAM_ONE_SECOND(3),
    TEAM_TWO_SECOND(4);

    private int value;

    Team(int value)
    {
        this.value = value;
    }
}
