package com.isoft.belot.domain.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.isoft.belot.domain.entities.enums.GameStatus;
import com.isoft.belot.domain.entities.enums.GameType;
import com.isoft.belot.domain.entities.enums.Team;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 * Belot entity. Could be created and joined from every player.
 * If all players leave a belot it gets removed.
 * <p>
 * {@link #belotPlayers} Set of max 4 players.
 * {@link #gameType    } Enum [PRIVATE_GAME,RANKED_GAME]
 * {@link #gameStatus  } Enum [WAITS_FOR_PLAYERS,STARTING,IN_PROGRESS,FIRST_TEAM_WON,SECOND_TEAM_WON]
 * {@link #dealings    } @OneToMany until one of the teams made 151 or more points.
 */
@Data
@Entity
@Table(name = "belots")
public class Belot extends BaseCreatedEntity implements Serializable
{
    @OneToMany(mappedBy = "belot", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Size(max = 4)
    @JsonManagedReference
    private Set<BelotPlayer> belotPlayers;

    @Enumerated(EnumType.STRING)
    private GameType gameType;

    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    @OneToMany(mappedBy = "belot", fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @LazyCollection(LazyCollectionOption.FALSE)
    @OrderBy(value = "created")
    private Set<Dealing> dealings;

    /**
     * create teams_points table in DB
     * key must be 0 for 1st team and 1 for 2nd team
     * value are the total points
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "teams_points", joinColumns = @JoinColumn(name = "id"))
    @MapKeyColumn(name = "team")
    @Column(name = "points")
    private Map<Integer, Integer> teamsPoints;

    /**
     * we don't need to assign all the fields because if 4 players don't join, the belot will be deleted.
     */
    public Belot()
    {
        this.belotPlayers = new HashSet<>();
        setCreated(LocalDateTime.now());
        this.gameStatus = GameStatus.WAITS_FOR_PLAYERS;
    }

    /**
     * only when 4 players join we can initialize the rest of the fields.
     * and change GameStatus to STARTING
     */
    public void readyToStart()
    {
        this.dealings = new LinkedHashSet<>();
        dealings.add(new Dealing());
        this.getDealings().iterator().next().setBelot(this);
        this.getDealings().iterator().next().setCreated(LocalDateTime.now());
        this.teamsPoints = new HashMap<>();
        this.gameStatus = GameStatus.STARTING;
    }

    /**
     * @return sorted Dealings by Date
     */
    @Transient
    public Set<Dealing> getSortedDealing()
    {
        return dealings.stream().sorted(comparing(Dealing::getCreated)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public BelotPlayer getTeamMate(BelotPlayer u)
    {
        if (!this.belotPlayers.contains(u) || u.getTeam().ordinal() == 0)
            return null;

        if (u.getTeam().ordinal() % 2 == 0)
            return this.belotPlayers.stream().filter(usr ->
                    !usr.equals(u) && usr.getTeam().ordinal() % 2 == 0)
                    .findFirst().orElse(null);
        else
            return this.belotPlayers.stream().filter(usr ->
                    !usr.equals(u) && usr.getTeam().ordinal() % 2 != 0)
                    .findFirst().orElse(null);
    }

    /**
     * Return the enemies of the given player as parameter
     * their order in the array will be the same as in the game
     * Guaranteed array with 2 elements, so no NullPointerExceptions are thrown during templating
     *
     * @param player the player
     * @return the array of enemies ordered
     */
    public BelotPlayer[] getEnemies(BelotPlayer player)
    {

        if (player.getTeam().ordinal() % 2 == 0)
        {
            return belotPlayers.stream()
                    .filter(usr -> usr.getTeam().ordinal() % 2 != 0)
                    .sorted(Comparator.comparingInt(usr -> usr.getTeam().ordinal()))
                    .toArray(withSize -> new BelotPlayer[2]);
        } else
        {
            return belotPlayers.stream()
                    .filter(usr -> usr.getTeam().ordinal() % 2 == 0)
                    .sorted(Comparator.comparingInt(usr -> usr.getTeam().ordinal()))
                    .toArray(withSize -> new BelotPlayer[2]);
        }
    }

    public void assignTeams()
    {
        final int[] i = {1};
        belotPlayers.forEach(player -> player.setTeam(Team.values()[i[0]++]));
    }

    @Transient
    public BelotPlayer getFirst()
    {
        return belotPlayers.stream()
                .filter(p -> p.getTeam().equals(Team.TEAM_ONE_FIRST))
                .findAny()
                .orElse(
                        belotPlayers.toArray(new BelotPlayer[0])[0]);
    }

    @Transient
    public BelotPlayer getNext(BelotPlayer player)
    {
        return belotPlayers.stream()
                .filter(p -> p.getTeam().ordinal() == player.getTeam().ordinal() + 1)
                .findFirst()
                .orElse(getFirst());
    }
}
