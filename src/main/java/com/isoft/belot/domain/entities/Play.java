package com.isoft.belot.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isoft.belot.domain.entities.enums.Declaration;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * every Play must have Card(mandatory) and could have Declarations(optional)
 */
@Data
@Entity
@Table(name = "plays")
public class Play extends Move
{
    @Transient
    private Card card;

    @Transient
    @JsonIgnore
    private List<Declaration> declarations;
}
