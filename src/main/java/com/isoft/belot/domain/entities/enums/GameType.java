package com.isoft.belot.domain.entities.enums;

/**
 * Game could be private(custome) where you can pick a teammate yourself
 * and ranked(random) where you gather points for a rankings.
 * Not an entity.
 */
public enum GameType
{
    PRIVATE_GAME,
    RANKED_GAME
}
