package com.isoft.belot.domain.entities;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Role implements org.springframework.security.core.GrantedAuthority
 * Role could be :
 * ROOT(unique - the first registered user) - could set other roles
 * ADMIN - could set other roles except ROOT role,
 * MODERATOR,
 * USER - every new registered player after the first one.
 */
@Entity
@Table(name = "roles")
public class Role extends BaseEntity implements GrantedAuthority
{
    private String authority;

    public Role(){}

    @Override
    @Column(name = "authority", nullable = false)
    public String getAuthority()
    {
        return authority;
    }

    public void setAuthority(String authority)
    {
        this.authority = authority;
    }
}
