package com.isoft.belot.domain.entities.enums;

/**
 * 9 Announce Type.
 * Not an Entity.
 */
public enum AnnounceType
{
    CLUBS(1),
    DIAMONDS(2),
    HEARTS(3),
    SPADES(4),
    NO_TRUMPS(5),
    ALL_TRUMPS(6),
    PASS(0),
    DOUBLE(-1),
    RE_DOUBLE(-2);

    public final int order;

    private AnnounceType(int i)
    {
        order = i;
    }

}
