package com.isoft.belot.domain.entities.enums;

/**
 * Magic Cards are unique for Magic Belot. They are extra cards(from the original Belot game)
 * you can use them during the announce phase.
 * <p>
 * {@link #BLIZZARD_STORM } After your play Blizzard Storm Magic card,
 * nobody can announce any further. You play this magic card AFTER
 * your own AnnounceType.
 * {@link #DESTINY_CREATOR} After your play Destiny Creator Magic card,
 * you draw 6th card. You play this magic card BEFORE your
 * own AnnounceType.
 *
 * @see AnnounceType
 * Not an Entity.
 */
public enum MagicCard
{
    BLIZZARD_STORM,
    DESTINY_CREATOR
}
