package com.isoft.belot.domain.entities;

import com.isoft.belot.domain.entities.enums.CardValue;
import com.isoft.belot.domain.entities.enums.Suit;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Objects;

/**
 * Magic Belot is played with 32 different cards. From 7 to A and
 * the 4 suits - Enum[Clubs♣, Diamonds♦, Hearts♥, Spades♠]
 * Not an Entity.
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Card
{
    @Enumerated(EnumType.STRING)
    private Suit suit;

    @Enumerated(EnumType.STRING)
    private CardValue cardValue;
}
