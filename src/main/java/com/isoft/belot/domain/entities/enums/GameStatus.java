package com.isoft.belot.domain.entities.enums;

/**
 * Every game is in one of these 5 phases.
 * FIRST_TEAM_WON and SECOND_TEAM_WON means the game is over.
 * Not an entity.
 */
public enum GameStatus
{
    WAITS_FOR_PLAYERS,
    STARTING,
    IN_PROGRESS,
    FIRST_TEAM_WON,
    SECOND_TEAM_WON
}
