package com.isoft.belot.domain.entities;

import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * MappedSuperclass Entity which Announce and Play extends
 * every belotplayer's Move has belotplayer, dealing and localdatetime
 *
 * @see Announce
 * @see Play
 */
@EqualsAndHashCode(callSuper = true)
@Data
@MappedSuperclass
public abstract class Move extends BaseCreatedEntity
{
    @ManyToOne(cascade = CascadeType.MERGE)@JoinColumn(name = "dealing_id", referencedColumnName = "id", nullable = false, updatable = false)@NotNull
    private Dealing dealing;

    @ManyToOne@JoinColumn(name = "belot_player_id", nullable = false)
    private BelotPlayer belotPlayer;

    protected Move(){}
}
