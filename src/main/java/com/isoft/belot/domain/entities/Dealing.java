package com.isoft.belot.domain.entities;

import com.google.common.collect.Iterables;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

/**
 * Every belot has List<Dealing> dealings till one of the teams made 151 points.
 * Then the belot game ends and could be removed from the active list ot belots.
 * Every Dealing has 2 Phases :
 * 1. Announce Phase - until 3 consecutive PASS are played.
 * 2. Play Phase - until all 32 Cards are played.
 *
 * @see Announce an AnnounceType(mandatory) and a MagicCard(optional)
 * @see Play    a Card(mandatory) and a Declaration(optional)
 * belot - ManyToOne relation with belot_id
 */
@Setter
@Getter
@Entity
@Table(name = "dealings")
public class Dealing extends BaseCreatedEntity
{
    @OneToMany(mappedBy = "dealing", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Announce> announces;

    @OneToMany(mappedBy = "dealing", fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Play> plays;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "belot_id", referencedColumnName = "id", nullable = false)
    private Belot belot;

    public Dealing()
    {
        this.announces = new LinkedHashSet<>();
        this.plays = new LinkedHashSet<>();
    }

    /**
     * @return sorted Set<Announces> chronologically
     */
    @Transient
    public Set<Announce> getSortedAnnounces()
    {
        return announces.stream().sorted(comparing(Announce::getCreated)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * checks whether the last two elements of the list<Announces> are PASS
     *
     * @return true or false
     */
    public boolean twoConsecutivePass()
    {
        final int size = announces.size();
        final int NEEDED_CONSECUTIVE_PASSES = 2;

        /**
         * size must be at least 3 cause otherwise when 3 PASSes are announced in the beginning
         * of Announce Phase that method would return true
         */
        final int NEEDED_SIZE = 3;

        return size >= NEEDED_SIZE &&
                getSortedAnnounces().stream()
                        .skip(size - NEEDED_CONSECUTIVE_PASSES)
                        .allMatch(announce ->
                                announce.getAnnounceType().equals(AnnounceType.PASS));
    }

    /**
     * checks whether there are exactly 3 elements in the list<Announces>
     * and all of them are PASS
     *
     * @return true or false
     */
    public boolean threeConsecutivePass()
    {
        return announces.size() == 3 &&
                announces.stream().allMatch(announce -> announce.getAnnounceType().equals(AnnounceType.PASS));
    }

    /**
     * @return the last announce that is NOT double or redouble or pass
     * and NULL, if there is none (only pass, or this is first announce)
     */
    @Nullable
    public Announce lastSuitAnnounce()
    {
        try
        {
            return Iterables.getLast(
                    getSortedAnnounces()
                            .stream()
                            .filter
                                    (ann -> ann.getAnnounceType().order > 0)
                            .collect(Collectors.toList()));
        } catch (NoSuchElementException e)
        {
             return null;
        }
    }
}

