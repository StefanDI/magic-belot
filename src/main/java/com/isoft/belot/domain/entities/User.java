package com.isoft.belot.domain.entities;

import lombok.EqualsAndHashCode;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * User entity for every new registered person in the app.
 * The first user's Role is set to ROOT and every other is set to USER
 *
 * @author Stefan Ivanov
 */
@EqualsAndHashCode
@Entity
@Table(name = "users")
public class User extends BaseEntity implements UserDetails, Serializable
{
    private String username;
    private String password;
    private String email;
    private String sessionId;
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;

    @ManyToMany(targetEntity = Role.class, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> authorities;

    //TODO Profile profile - picture, rankingPoints, gold(for shop), names

    public User()
    {
        this.authorities = new HashSet<>();
    }

    public User(String username,
                String password,
                String email,
                String sessionId,
                boolean isAccountNonExpired,
                boolean isAccountNonLocked,
                boolean isCredentialsNonExpired,
                boolean isEnabled,
                Set<Role> authorities)
    {
        this.username = username;
        this.password = password;
        this.email = email;
        this.sessionId = sessionId;
        this.isAccountNonExpired = isAccountNonExpired;
        this.isAccountNonLocked = isAccountNonLocked;
        this.isCredentialsNonExpired = isCredentialsNonExpired;
        this.isEnabled = isEnabled;
        this.authorities = authorities;
    }

    @Override
    @Column(name = "username", nullable = false, unique = true, updatable = false)
    @NotNull
    @Size(min = 3, max = 20)
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    @Override
    @Column(name = "password", nullable = false)
    @NotNull
    @Size(min = 6)
    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Column(name = "email", unique = true, nullable = false, updatable = false)
    @NotNull
    @Email
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Override
    @Column(name = "is_account_non_expired")
    public boolean isAccountNonExpired()
    {
        return true;
    }

    public void setAccountNonExpired(boolean accountNonExpired)
    {
        isAccountNonExpired = accountNonExpired;
    }

    @Override
    @Column(name = "is_account_non_locked")
    public boolean isAccountNonLocked()
    {
        return true;
    }

    public void setAccountNonLocked(boolean accountNonLocked)
    {
        isAccountNonLocked = accountNonLocked;
    }

    @Override
    @Column(name = "is_credentials_non_expired")
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired)
    {
        isCredentialsNonExpired = credentialsNonExpired;
    }

    @Override
    @Column(name = "is_enabled")
    public boolean isEnabled()
    {
        return true;
    }

    public void setEnabled(boolean enabled)
    {
        isEnabled = enabled;
    }

    @Column(name = "sessionId")
    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public Set<Role> getAuthorities()
    {
        return this.authorities;
    }

    public void setAuthorities(Set<Role> authorities)
    {
        this.authorities = authorities;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username) &&
                email.equals(user.email);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(username, email);
    }
}
