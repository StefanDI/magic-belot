package com.isoft.belot.domain.entities;

import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.MagicCard;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * The first stage of Magic Belot game is Announce Stage.
 * Players should play AnnounceType(mandatory)
 * and MagicCard(optional)
 * Players plays this stage till three consecutive PASS(AnnounceType)
 * <p>
 * {@link #magicCard   } EnumType.STRING
 * {@link #announceType} EnumType.STRING
 *
 * @see com.isoft.belot.domain.entities.enums.AnnounceType
 * @see com.isoft.belot.domain.entities.enums.MagicCard
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "announces")
public class Announce extends Move
{
    @Enumerated(EnumType.STRING)
    private MagicCard magicCard;

    @Enumerated(EnumType.STRING)
    private AnnounceType announceType;

    private boolean isMagicCardPlayed;

    @Transient
    public boolean isMagicCardPlayed()
    {
        return isMagicCardPlayed;
    }

    public void setMagicCardPlayed(boolean magicCardPlayed)
    {
        isMagicCardPlayed = magicCardPlayed;
    }

}
