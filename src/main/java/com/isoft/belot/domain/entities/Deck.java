package com.isoft.belot.domain.entities;

import com.isoft.belot.domain.entities.enums.CardValue;
import com.isoft.belot.domain.entities.enums.Suit;
import lombok.Getter;

import java.util.*;

/**
 * Every deck has 32 different cards [7,8,9,10,J,Q,K,A][Clubs♣, Diamonds♦, Hearts♥, Spades♠]
 * the constructor creates a List with capacity 32, fill it with 32 different cards
 * and randomize(shuffle the cards) with Collections.shuffle method
 * Not an Entity.
 */
public class Deck
{
    //TODO не трябва ли да е unmodifialbe list, защото ако играят две 4ки белот, може от едното тесте като изгори карта да изгаря и във второто?
    @Getter
    private List<Card> cards;

    private static final Map<Belot, Deck> belotDecks = new HashMap<>();

    public Deck(Belot b)
    {
        this();
        belotDecks.put(b, this);
    }

    public Deck()
    {
        this.cards = new ArrayList<>(32);
        this.fill();
        this.shuffle();
    }

    /**
     * removes all the elements from the list
     * and fill it with 32 different Cards from 7-ACE and 4 Suits.
     */
    private void fill()
    {
        for (Suit suit : Suit.values())
        {
            for (CardValue value : CardValue.values())
            {
                Card card = new Card(suit, value);
                this.cards.add(card);
            }
        }
    }

    private void shuffle()
    {
        Collections.shuffle(this.cards);
    }

    /**
     * removes and
     *
     * @return the first card
     * Since they are shuffled it's a random card.
     * @see #shuffle()
     */
    public Card drawCard()
    {
        return this.cards.remove(0);
    }

    public int getSize()
    {
        return cards.size();
    }

    public static Deck getDeck(Belot b)
    {
        return belotDecks.getOrDefault(b, new Deck());
    }
}
