package com.isoft.belot.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Stefan Ivanov
 */
@Controller
public class HomeController extends BaseController
{
    @GetMapping("/")
    public ModelAndView showHome()
    {
        return view("index");
    }

}
