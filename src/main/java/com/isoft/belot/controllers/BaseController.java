package com.isoft.belot.controllers;

import org.springframework.web.servlet.ModelAndView;

/**
 * This is a base controller, that all other controllers inherit(extend)
 * It has base methods for showing a view and redirecting
 *
 * @author Stefan Ivanov
 */
public abstract class BaseController
{

    private static final String REDIRECT = "redirect:";

    protected ModelAndView view(String view, ModelAndView modelAndView)
    {
        modelAndView.setViewName(view);

        return modelAndView;
    }

    protected ModelAndView view(String view)
    {
        return this.view(view, new ModelAndView());
    }

    protected ModelAndView redirect(String url)
    {
        return this.view(REDIRECT + url);
    }
}