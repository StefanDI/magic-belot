package com.isoft.belot.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.services.game.GameServiceFactory;
import com.isoft.belot.services.interfaces.MessageHandlingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;

import java.security.Principal;

/**
 * @author Stefan Ivanov
 */
@Slf4j
@Controller
public class BelotController extends BaseController
{
    private final GameServiceFactory serviceFactory;

    @Autowired
    public BelotController(final GameServiceFactory serviceFactory)
    {
        this.serviceFactory = serviceFactory;
    }

    /**
     * This is invoked when the client sends a message through websocket at the specific endpoint
     * calls the {@link MessageHandlingService} to handle the incoming message
     * Usually used by clients in the same belot game
     *
     * @param belotId the id of the belot game
     * @param msg     the message from the client
     * @param user    the user who invoked it
     */
    @MessageMapping("/belot/{belotId}")
    public void handleGameSpecificRequest(
            @DestinationVariable String belotId,
            @Payload SocketClientMessage msg,
            Principal user) throws JsonProcessingException
    {
        handleRequest(belotId, msg, user);
    }

    /**
     * This is invoked when the client sends a message through websocket at the specific endpoint
     * calls the {@link MessageHandlingService} to handle the incoming message
     * Usually used by clients not in the same belot game
     *
     * @param message the message from the client
     * @param user    the user who invoked it
     */
    @MessageMapping("/belot/")
    public void handleGlobalRequest(@Payload SocketClientMessage message, Principal user) throws JsonProcessingException
    {
        handleRequest(null, message, user);
    }


    private void handleRequest(@Nullable String belotId,
                               SocketClientMessage msg,
                               Principal user) throws JsonProcessingException
    {
        //Attach sender to the message
        User sender = (User) ((UsernamePasswordAuthenticationToken) user).getPrincipal();
        msg.setSender(sender);

        try
        {
            serviceFactory.setClientMessage(msg);
            serviceFactory.setBelotId(belotId);
            MessageHandlingService messageHandler = serviceFactory.createInstance();
            messageHandler.processMessage();
        } catch (Exception e)
        {
            log.error("AbstractGameService.createInstance() thrown Exception", e);
        }
    }
}
