package com.isoft.belot.controllers;

import com.isoft.belot.domain.entities.User;
import com.isoft.belot.domain.models.binding.UserBinding;
import com.isoft.belot.domain.models.view.UserView;
import com.isoft.belot.services.interfaces.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController extends BaseController
{
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService,
                          ModelMapper modelMapper)
    {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }


    @PostMapping("/register")
    public ModelAndView registerConfirm(@Valid @ModelAttribute UserBinding model)
    {
        if (!model.getPassword().equals(model.getConfirmPassword()))
        {
            return view("register");
        }
        this.userService.add(model);
        return redirect("/");
    }

    /**
     * @return the current logged in user or null if not logged in
     */
    @RequestMapping("/users/me")
    @ResponseBody
    public UserView getLoggedInUser()
    {

        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser"))
            return null;
        else
        {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            //For some reason Spring SecurityContextHolder, doesnt hold the userId, so i need to fetch it manually
            user.setId(this.userService.findByUsername(user.getUsername()).getId());
            UserView userViewModel = new UserView();
            modelMapper.map(user, userViewModel);

            return userViewModel;
        }
    }

    @RequestMapping("/username_exists")
    @ResponseBody
    public String usernameExists(@RequestParam String username)
    {

        if (this.userService.usernameExists(username))
            return "exists";
        else
            return "not_exists";
    }

    @RequestMapping("/email_exists")
    @ResponseBody
    public String emailExists(@RequestParam String email)
    {
        if (this.userService.emailExists(email))
            return "exists";
        else
            return "not_exists";
    }


    //DEVELOPMENT ONLY
    @RequestMapping("/get_all")
    @ResponseBody
    public List<String> getUsernames() {
       return this.userService.findAll()
               .stream()
               .map(User::getUsername)
               .collect(Collectors.toList());
    }

}