package com.isoft.belot.config;

import com.google.common.collect.ImmutableList;
import com.isoft.belot.handlers.LoginFailureHandler;
import com.isoft.belot.handlers.LoginSuccessHandler;
import com.isoft.belot.handlers.LogoutHandler;
import com.isoft.belot.handlers.SessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * Main Security Configuration Class
 * includes cors, csrf, authorization, authentication, login, logout, error
 *
 * DO NOT AUTO FORMAT!!!
 *
 * @author Stefan Ivanov
 */
@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
    private final LoginSuccessHandler successHandler;
    private final LoginFailureHandler failureHandler;
    private final LogoutSuccessHandler logoutHandler;
    private final SessionRegistry sessionRegistry;

    @Autowired
    public SecurityConfig(LoginSuccessHandler successHandler,
                          LoginFailureHandler failureHandler,
                          LogoutHandler logoutHandler,
                          SessionHandler sessionRegistry
                          )
    {
        this.successHandler = successHandler;
        this.failureHandler = failureHandler;
        this.logoutHandler = logoutHandler;
        this.sessionRegistry = sessionRegistry;
    }



    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http
                .sessionManagement()
                .enableSessionUrlRewriting(true)
//                .maximumSessions(1).sessionRegistry(sessionRegistry).and()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .cors().and()
                //TODO: Enable CSRF and give token to all ajax request from front end
                .csrf().disable()
//                .csrfTokenRepository(csrfTokenRepository())
//                .and()
                .authorizeRequests()
                .antMatchers("/", "/login", "/register", "/callback/", "/webjars/**", "/error**").permitAll()
                .antMatchers("/js/**", "/css/**").permitAll()
                .antMatchers(HttpMethod.GET, "/username_exists**", "/email_exists**", "/get_all**").permitAll()
                .antMatchers(HttpMethod.GET, "/users/me").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/")
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                .usernameParameter("username")
                .passwordParameter("password")
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessHandler(logoutHandler)
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/")
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/unauthorized").and()
                .headers().frameOptions().disable();
        ;

    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(ImmutableList.of("*"));
        configuration.setAllowedMethods(ImmutableList.of("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(ImmutableList.of("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    private CsrfTokenRepository csrfTokenRepository()
    {
        HttpSessionCsrfTokenRepository repository =
                new HttpSessionCsrfTokenRepository();
        repository.setSessionAttributeName("_csrf");
        return repository;
    }
}
