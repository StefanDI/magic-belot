package com.isoft.belot.config;

import com.isoft.belot.interceptors.SessionHandshakeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.socket.server.HandshakeInterceptor;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

/**
 * @author Stefan Ivanov
 */
@Component
public class InterceptorConfig implements WebMvcConfigurer
{
    private final HandlerInterceptor sessionHandshakeInterceptor;

    @Autowired
    public InterceptorConfig(SessionHandshakeInterceptor sessionHandshakeInterceptor)
    {
        this.sessionHandshakeInterceptor = sessionHandshakeInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(sessionHandshakeInterceptor);
    }
}
