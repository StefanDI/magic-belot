package com.isoft.belot.config;

import com.isoft.belot.util.ValidatorUtilImpl;
import com.isoft.belot.util.interfaces.ValidatorUtil;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.validation.Validation;
import javax.validation.Validator;

/**
 * Bean configuration class
 * #modelMapper() for deep copy of objects's fields
 * #encoder() to encrypt a String password to hash(password) - non reversible
 * #validator() to check if there are errors in the mappings
 * #validationUtil() to call the validator() method
 */
@Configuration
public class ApplicationBeanConfig
{
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

    @Bean
    public BCryptPasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Validator validator() {
        return Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Bean
    public ValidatorUtil validationUtil() {
        return new ValidatorUtilImpl(validator());
    }
}