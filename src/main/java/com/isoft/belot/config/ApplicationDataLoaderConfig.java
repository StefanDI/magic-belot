package com.isoft.belot.config;

import com.isoft.belot.domain.entities.User;
import com.isoft.belot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import static com.isoft.belot.util.DummyUsersGenerator.emails;
import static com.isoft.belot.util.DummyUsersGenerator.names;

@Component
public class ApplicationDataLoaderConfig implements ApplicationRunner
{
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public ApplicationDataLoaderConfig(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder)
    {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception
    {
        if(userRepository.count() >= names.length)
            return;

        for (int i = 0; i < names.length; i++)
        {
            User u = new User();
            u.setUsername(names[i]);
            u.setEmail(emails[i]);
            String encodedPassword = passwordEncoder.encode("password");
            u.setPassword(encodedPassword);

            userRepository.save(u);
        }
    }
}
