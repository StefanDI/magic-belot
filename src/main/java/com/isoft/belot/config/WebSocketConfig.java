package com.isoft.belot.config;

import com.isoft.belot.services.interfaces.BelotPlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

/**
 * @author Stefan Ivanov
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    private final BelotPlayerService playerService;


    @Autowired
    public WebSocketConfig(BelotPlayerService playerService) {
        this.playerService = playerService;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic/belot-game", "/user/topic/belot-game");
        config.setApplicationDestinationPrefixes("/");

    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        //fallback if the browser doesn't support SockJS
        registry.addEndpoint("/belot").setAllowedOrigins("*");
        registry.addEndpoint("/belot").setAllowedOrigins("*").withSockJS();
    }

    @EventListener
    public void onSocketDisconnected(SessionDisconnectEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        System.out.println("[Disonnected] " + sha.getSessionId());
        //        if(event.getUser() != null )
//        if(event.getUser() != null )
//        {
//            playerService.removeByUsername(event.getUser().getName());
//        }
    }
    //1D570053B260E8560F4D58D2B7F83715
    //94F72DEEC4DAC4655EF9B42A686CC68F

    @EventListener
    public void handleSessionConnected(SessionConnectEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        String sessionId = sha.getSessionId();
        System.out.println(event.getSource());
        System.out.println("eveeent");
    }

}
