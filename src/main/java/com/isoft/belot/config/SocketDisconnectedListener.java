package com.isoft.belot.config;

import org.springframework.messaging.Message;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.security.Principal;

/**
 * @author Stefan Ivanov
 */
public class SocketDisconnectedListener extends SessionDisconnectEvent
{
    public SocketDisconnectedListener(Object source, Message<byte[]> message, String sessionId, CloseStatus closeStatus, Principal user)
    {
        super(source, message, sessionId, closeStatus, user);
    }
}
