/**
 * performs validation before submitting the form
 * @type {number}
 * TODO: make this into a class or IIFE for not polluting the global namespace
 */
const USERNAME_MIN_SIZE = 3;
const USERNAME_MAX_SIZE = 20;
const USERNAME_VALIDATION_REGEX = "^[a-zA-Z0-9_\\-]+$";
const PASSWORD_MIN_LENGTH = 6;
const EMAIL_EXISTS_URL = "/email_exists";
const USERNAME_EXISTS_URL = "/username_exists";
const LOADING_ID = "load_id";

function shakeModal() {
    $("#modal-register").addClass("shake");
    setTimeout(() => {
        $("#modal-register").removeClass("shake");
    }, 1000)
}

function isRegisterFormValidated() {
    let isValidUsername = validateUsername();
    let isValidPass = validatePassword();
    let isValidEmail = validateEmail();

    if (!isValidUsername || !isValidPass || !isValidEmail) {
        shakeModal();
    } else {
        //Synchronous call of asynchronous promise execution :D
        //Basicaly chain promises if usernameExists and emailExists submit form, else make it shake
        usernameExists()
            .then(() => {
                emailExists()
                    .then(() => {
                        $("#register-form").submit();
                    })
                    .catch(() => {
                        shakeModal();
                    })
            })
            .catch(() => {
                shakeModal()
            })
    }

}
// if (res == "success") {

// } else {
//     $('#register-form-password').addClass('invalid').removeClass('valid');
//     $('#password_label').attr({'data-error': res, 'data-success': ''});
// }

function validateUsername(e) {
    let username = e ? e.target.value : $("#register-username").val();
    let usernameError = $("#username-error");
    usernameError.text("");
    if (!username || username.length === 0) {
        // $('#register-username').addClass('invalid').removeClass('valid');
        // $('#register-username-label').attr({'data-success': '', 'data-error': ErrorMessages.USERNAME_NOT_NULL});
        return false;
    } else if (username.length < USERNAME_MIN_SIZE) {
        $('#register-username').addClass('invalid').removeClass('valid');
        $('#register-username-label').attr({'data-success': '', 'data-error': ErrorMessages.USERNAME_TOO_SMALL});
        return false;
    } else if (username.length > USERNAME_MAX_SIZE) {
        $('#register-username').addClass('invalid').removeClass('valid');
        $('#register-username-label').attr({'data-success': '', 'data-error': ErrorMessages.USERNAME_TOO_BIG});
        return false;
    } else if (!username.match(USERNAME_VALIDATION_REGEX)) {
        $('#register-username').addClass('invalid').removeClass('valid');
        $('#register-username-label').attr({'data-success': '', 'data-error': ErrorMessages.USERNAME_ILLEGAL_CHARACTERS});
        return false;
    } else {
        //If this is called from the input onBlur this will execute
        //otherwise i want to call it differently
        if (e)
            usernameExists();
    }

    return true;
}

function usernameExists() {
    return new Promise((resolve, reject) => {
        let username = $("#register-username").val();
        Loading.startLoading(LOADING_ID, ".register-username");
        isUsernameAlreadyInUse(username)
            .then(() => {
                resolve();
            })
            .catch(() => {
                reject();
            });
    });

}

function validateEmail(e) {
    let email = e ? e.target.value : $("#register-email").val();
    let emailError = $("#email-error");

    emailError.text("");

    if (!email || email.length === 0) {
        // $('#register-email').addClass('invalid').removeClass('valid');
        // $('#register-email-label').attr({'data-success': '', 'data-error': ErrorMessages.EMAIL_NOT_NULL});
        return false;
    } else if (!validateEmailRegex(email)) {
        $('#register-email').addClass('invalid').removeClass('valid');
        $('#register-email-label').attr({'data-success': '', 'data-error': ErrorMessages.INVALID_EMAIL});
        return false;
    } else {
        //If this is called from the input onBlur this will execute
        //otherwise i want to call it differently
        if (e)
            emailExists();
    }
    return true;
}

function emailExists() {
    return new Promise((resolve, reject) => {
        let email = $("#register-email").val();
        Loading.startLoading(LOADING_ID, ".register-email");

        isEmailAlreadyInUse(email).then(() => {
            resolve();
        }).catch(() => {
            reject();
        })
    });
}

function validatePassword(e) {
    let password = $("#register-password").val();

    let confirmPassword = $("#register-confirm-password").val();


    if(password === '' && confirmPassword === '')
        return false;
    else if (password !== confirmPassword) {
        $('#register-confirm-password, #register-password').addClass('invalid').removeClass('valid');
        $('#register-confirm-password-label, #register-password-label').attr({'data-success': '', 'data-error': ErrorMessages.PASSWORDS_DONT_MATCH});
        return false;
    }
    else if (password.length < PASSWORD_MIN_LENGTH ) {
        $('#register-confirm-password').addClass('invalid').removeClass('valid');
        $('#register-confirm-password-label').attr({'data-success': '', 'data-error': ErrorMessages.PASSWORD_TOO_SHORT});
        return false;
    }

    if (password === confirmPassword) {
        $('#register-confirm-password, #register-password').addClass('valid').removeClass('invalid');
        $('#register-confirm-password-label, #register-password-label').attr({'data-success': 'top-notch password', 'data-error': ''});
    }
    return true;
}


function validateEmailRegex(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


function isEmailAlreadyInUse(email) {
    return new Promise((resolve, reject) => {
        $.get({
            url: EMAIL_EXISTS_URL,
            data: "email=" + email,
            complete: (response) => {
                Loading.stopLoading(LOADING_ID);

                if (response.responseText === "exists") {
                    $('#register-email').addClass('invalid').removeClass('valid');
                    $('#register-email-label').attr({'data-success': '', 'data-error': ErrorMessages.EMAIL_TAKEN});
                    reject("reject");
                } else {
                    $('#register-email').addClass('valid').removeClass('invalid');
                    $('#register-email-label').attr({'data-success': SuccessMessages.EMAIL_NOT_TAKEN, 'data-error': ''});
                    resolve("resolve");
                }
            }
        })
    });

}

function isUsernameAlreadyInUse(username) {
    return new Promise((resolve, reject) => {
        $.get({
            url: USERNAME_EXISTS_URL,
            data: "username=" + username,
            complete: (response) => {
                Loading.stopLoading(LOADING_ID);
                if (response.responseText === "exists") {
                    $('#register-username').addClass('invalid').removeClass('valid');
                    $('#register-username-label').attr({'data-success': '', 'data-error': ErrorMessages.USERNAME_TAKEN});
                    reject();
                } else {
                    $('#register-username').addClass('valid').removeClass('invalid');
                    $('#register-username-label').attr({'data-success': SuccessMessages.USERNAME_NOT_TAKEN, 'data-error': ''});
                    resolve();
                }
            }
        });
    });
}
