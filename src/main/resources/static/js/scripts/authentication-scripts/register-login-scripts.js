/**
 * Holds functions for login register and logout
 */
function loginSubmit(e) {
    e.preventDefault();
    Loading.startLoading(LOADING_ID, ".login-loading");

    $.post({
        url: "/login",
        data: $("#login-form").serialize(),
        success: function () {
            loggedInUser.getLoggedInUser();
            SnackBar.scheduleSnackbarAfterReload(SuccessMessages.LOGIN_SUCCESSFUL);
            Loading.stopLoading(LOADING_ID);

            location.reload();
        },
        error: function (response) {
            Loading.stopLoading(LOADING_ID);
            if (response.getResponseHeader("login-error")) {
                $("#login-error").text(response.getResponseHeader("login-error"));
            }
        }
    });
}

function registerSubmit(e) {
    e.preventDefault();

    let account = $("#register-username").val();
    let password = $("#register-password").val();
    const REGISTER_LOAD_ID = "loadId";

    Loading.startLoading(REGISTER_LOAD_ID, ".register-loading");
    $.post({
        url: "/register",
        data: $("#register-form").serialize(),
        success: function () {

            Loading.stopLoading(REGISTER_LOAD_ID);
            //The most simple way of doing it, simply trigger click on the button that closes the modal and shows the login modal
            //Otherwise we need to rework modal opening from scratch
            //and we will lose the fade effect, nicely given to us by bootstrap team
            $("#close-register-modal").trigger("click");
            $("#login").trigger("click");

            $("#login-username").val(account);
            $("#login-password").val(password);

            SnackBar.createSnackbar(SuccessMessages.REGISTER_SUCCESSFUL);
        },
        error: function () {
            Loading.stopLoading(REGISTER_LOAD_ID);
            //Shake the modal on error
            $("#register-form").addClass("shake");
            setTimeout(() => {
                $("#register-form").removeClass("shake");
            }, 1500)
        }
    })
}

function logout() {

    let logoutData = $("#logout_form").serialize();
    Loading.startLoading("loadId", ".loading-user");
    $.post({
        url: "/logout",
        data: logoutData,
        success: function () {
            SnackBar.scheduleSnackbarAfterReload(SuccessMessages.LOGOUT_SUCCESSFUL);

            Loading.stopLoading("loadId");

            loggedInUser.logout();

            location.reload();
        }
    })
}