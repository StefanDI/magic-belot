"use strict";
/**
 * This script should(ideally) contain code only inside ${document}.ready() function
 * binds event listeners to static elements, sets the logged in user, renders snackbar etc etc
 * this is javascript SCRIPT so no SOLID principles here (single responsibility) :D
 * SOLID, YOU HAVE NO POWER HERE!!!
 *
 * This script is used for binding of different listeners to elements
 * its called once when elements are rendered
 * NOTE: wont work for dynamically rendered elements
 *
 *
 */

window.onresize = () => debounce(renderer.resizeField("currentView"), 150);

// Debounce
function debounce(func, time) {
    let timeOut = time || 100; // 100 by default if no param
    let timer;
    return function (event) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(func, timeOut, event);
    };
}

$(document).ready(() => {

    // SideNav Button Initialization
    $(".button-collapse").sideNav({edge: 'right'});
    $('.collapsible').collapsible();
    // SideNav Scrollbar Initialization
    let sideNavScrollbar = document.querySelector('.custom-scrollbar');

    let ps = new PerfectScrollbar(sideNavScrollbar);

    showGameView();

    AppTheme.applyTheme();
    //The front end needs info on the current logged in user, ask the backend if
    //this client is authenticated and set the user
    if (!loggedInUser.isPresent)
        loggedInUser.getLoggedInUser();

    $(window).on('beforeunload', function () {
        if (loggedInUser.isPresent && loggedInUser.isInBelot)
            return "Are you sure you want to leave the game?";
        //TODO:ask for confirmation to exit only if gameSocket is open or the player is in a belot game
        //TODO: custom modal for this confirmation prompt
    });

    $(window).on('unload', function () {
        //This is only for the front end, it wont actually log out
        loggedInUser.logout();
    });

    /**
     * If i need to render a snackbar
     * to the user but the page is refreshed
     * i save the snackbar info in the localstorage, so every time i reload a page
     * check if the localstorage has item snackbar and render it to the user
     * {@see SnackBar} jsDoc for more info on what is snackbar (or just google it;)
     */
    if (localStorage.getItem("snackbar")) {
        SnackBar.createSnackbar(localStorage.getItem("snackbar-message"));
        localStorage.removeItem("snackbar");
        localStorage.removeItem("snackbar-message");
    }

    /**
     * blur event is fired, when an <input> element which was focused (clicked)
     * loses focus
     *  So basically when these inputs lose focus, validate if they are correct
     */
    $("#register-username").blur(validateUsername);
    $("#register-email").blur(validateEmail);
    $("#register-password").blur(validatePassword);
    $("#register-confirm-password").blur(validatePassword);

    /**
     * manually submit the register form to handle loading and redirect
     */
    $("#register-form").submit((e) => {
        registerSubmit(e);
    });

    /**
     * manually submit login form
     */
    $("#login-form").submit((e) => {
        loginSubmit(e);
    });

    //TODO: this is experimental and needs rework and rethink
    //TODO: every other view must be hidden
    $("#play")[0].addEventListener("click", () => {
        showInitialGameScreen();
    });

    $('.switch #themeSwitch').on('change', function (e) {
        if (e.target.checked)
            AppTheme.setTheme("default");
        else
            AppTheme.setTheme("dark");


        AppTheme.applyTheme();
    });


    $("#readyToPlay")[0].addEventListener('click', () => {
        //TODO: not close the modal but show how many players accepted
        showGameView().then(() => {
            loggedInUser.gameSocket.sendMessage({type: "READY"}, "/belot/" + loggedInUser.belotId);
        });
    });

    let loginModal = $("#modal-login");
    loginModal.on("show.bs.modal", () => {
        $(".md-form input").each(function () {
            let text = this.value;
            this.value = "";
            this.value = text;
            this.focus();
            this.blur();
        })
    });
    loginModal.on("hide.bs.modal", () => {
        $(".md-form input").each(function () {
            this.value = "";
            this.blur();
        })
    });
});

/**
 * Sets the color to all the colored elements (box shadow, hover and everything)
 * based on the colors from {@link AppTheme}
 * DO NOT CHANGE THIS FUNCTION
 */



//All functions below, DONT belong here, i put functions here, which i haven't
// yet determined their place :D
//TODO: move to appropriate js file
//TODO: make like #showGameView()
function showInitialGameScreen() {
    let currentView = document.getElementById("currentView");
    $(currentView).empty();
    $(currentView).load("/html/fragments/game/initial-view.html", () => {
        AppTheme.applyTheme();
        let createPrivateGameBtn = document.getElementById("createGame");
        let joinPrivateGameBtn = document.getElementById("joinGame");
        let searchRankedBtn = document.getElementById("searchRanked");

        createPrivateGameBtn.addEventListener("click", () => {
            createGame();
        });

        joinPrivateGameBtn.addEventListener("click", () => {
            joinGame();
        });

        searchRankedBtn.addEventListener("click", () => {
            let isCanceled = false;
            loggedInUser.gameSocket.sendMessage({type: "SEARCH_RANKED"}, "/belot/");
            let searchRankedModal = "#searchRankedModal";
            $(searchRankedModal).modal({
                show: true
            });
            $(searchRankedModal).on("hidden.bs.modal", () => {
                // ($("element").data('bs.modal') || {})._isShown
                if (!isCanceled)
                    document.getElementById("minimizedGameSearch").style.display = "block";
            });
            $(".cancel-search").on("click", (e) => {
                isCanceled = true;
                if (e.target.parentNode.classList.contains("modal-body")) {
                    document.getElementById("searchRankedModal").style.display = "none";
                } else {
                    document.getElementById("minimizedGameSearch").style.display = "none";
                }
                loggedInUser.gameSocket.sendMessage({type: "CANCEL_SEARCH"}, "/belot/")
            });


        })
    });
}

function showGameView() {
    let currentView = document.getElementById("currentView");
    $(currentView).empty();

    return showView(currentView, "/html/fragments/game/game-template.html")
}

function showView(loadInto, url) {
    return new Promise((resolve, error) => {
        $(loadInto).load(url,
            (responseText, textStatus, req) => {
                if (textStatus === "error") {
                    error(responseText);
                } else {
                    AppTheme.applyTheme();
                    resolve();
                }
            });
    });
}

function showEditUserView() {
    $("#slidingDiv").load("/html/fragments/user/edit-user-info.html", () => {
        $("#slidingDiv")[0].style.top = "0px";
    })
}