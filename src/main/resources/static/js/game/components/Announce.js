let Announce = (function () {

    function createAnnounces(selector, announces) {
        let announceWrapper = document.createElement('div');
        announceWrapper.classList.add("announce-wrapper");
        announceWrapper.classList.add("app-primary-light");
        announceWrapper.classList.add("has-shadow");
        announceWrapper.classList.add("app-divider");

        function enableBtn() {
            document.getElementsByClassName("announce-btn")[0]
                .classList.remove("disabled");
        }

        let i = 0;
        for (let announcesKey in announces) {
            i++;
            let annRow = document.createElement("div");
            let annLabel = document.createElement("label");
            let radioBtn = document.createElement("input");

            annRow.classList.add("form-check");

            annLabel.classList.add("announce-row");
            annLabel.classList.add("form-check-label");
            annLabel.htmlFor = "radio" + i;

            if (Object.prototype.hasOwnProperty.call(announces, announcesKey)) {
                if (announces[announcesKey] !== true) {
                    radioBtn.setAttribute("disabled", "");
                }
            }

            annLabel.innerHTML = announcesKey;

            radioBtn.type = "radio";
            radioBtn.classList.add("form-check-input");
            radioBtn.id = "radio" + i;
            radioBtn.name = "announce-name";
            radioBtn.addEventListener('change', enableBtn);
            $(annRow).append(radioBtn);
            $(annRow).append(annLabel);
            $(announceWrapper).append(annRow);
        }
        let confirmBtn = document.createElement("button");
        confirmBtn.classList.add("btn");
        confirmBtn.classList.add("announce-btn");
        confirmBtn.classList.add("disabled");
        confirmBtn.classList.add("app-primary");
        confirmBtn.classList.add("app-text");
        confirmBtn.classList.add("waves-effect");
        confirmBtn.innerText = "Announce";
        confirmBtn.addEventListener('click', announceClicked);

        $(announceWrapper).append(confirmBtn);
        $(selector).append(announceWrapper);
    }

    return {
        createAnnounces: createAnnounces
    }
})();