let renderer = (function () {
    const PADDING = 40;//px
    function resizeContainer(container, handRect) {
        let viewPortWidth =
            Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        let topContainer = container.parentNode;
        let totalElementsWidth = 2 * PADDING + handRect.width + 2 * handRect.height;
        let spaceBetweenHands = (viewPortWidth - totalElementsWidth) < 250 ?
            200
            :
            (viewPortWidth - totalElementsWidth - viewPortWidth / 10) / 2;
        
        topContainer.style.width = totalElementsWidth + 2 * spaceBetweenHands + "px";
    }

    function resizeField(viewId) {
        let container = document.getElementById(viewId);
        let topCards = document.getElementsByClassName("partner")[0];
        let bottomCards = document.getElementsByClassName("you")[0];
        let leftCards = $(".players .left")[0];
        let rightCards = $(".players .right")[0];
        let handRect =
            document.getElementsByClassName("partner")[0] //partner, because its for sure horizontal
                .getBoundingClientRect();

        /*EXPERIMENTAL*/
        resizeContainer(container, handRect);

        let singleCardWidth =
            window.getComputedStyle(
                document.getElementsByClassName("belot-card")[0])
                .getPropertyValue("width")
                .replace("px", "");
        let handWidth = handRect.width;
        let handHeight = handRect.height;

        let containerWidth = document.getElementById(viewId).getBoundingClientRect().width;
        let containerHeight = 4 * PADDING + handWidth + handHeight * 2;

        container.style.height = containerHeight + "px";
        //equal to the right offset
        let leftOffset = (containerWidth - handWidth) / 2;
        let spaceBetweenRotatedCards = (containerWidth - handWidth - handHeight * 2 - PADDING * 2) / 2;

        topCards.style.left = leftOffset + "px";
        topCards.style.top = PADDING + "px";

        let widthOnRotate = handWidth - handHeight;
        leftCards.style.left = PADDING - widthOnRotate + "px";
        leftCards.style.top = 2 * PADDING + 2 * handHeight + (handWidth - handHeight) + "px";

        rightCards.style.left =
            PADDING - widthOnRotate + 2 * spaceBetweenRotatedCards + handHeight + handWidth + "px";
        rightCards.style.top = 2 * PADDING + 2 * handHeight + (handWidth - handHeight) + "px";


        bottomCards.style.left = leftOffset + "px";
        bottomCards.style.top = handWidth + 3 * PADDING + handHeight + "px";

        let announceWrapper =
            document.getElementsByClassName("announce-wrapper")[0];

        //Render the announces
        if (announceWrapper && announceWrapper.nodeType === Node.ELEMENT_NODE) {
            let announceBtn = document.getElementsByClassName("announce-btn")[0];
            let announceBtnWidth =
                window.getComputedStyle(announceBtn)
                    .getPropertyValue("width")
                    .replace("px", "");
            let announceWidth =
                window.getComputedStyle(announceWrapper)
                    .getPropertyValue("width")
                    .replace("px", "");

            let announceLeftOffset = (containerWidth - announceWidth) / 2;

            announceWrapper.style.left = announceLeftOffset + "px";
            announceWrapper.style.top = (3 * PADDING + handWidth) / 2 + "px";
            announceBtn.style.left = (announceWidth - announceBtnWidth) / 2 + "px";
            // bottomCards.style.top = bottomCards.style.top.replace("px", "") + 100 + "px";
        }

        AppTheme.applyTheme();
    }

    return {
        resizeField: resizeField
    }
})();