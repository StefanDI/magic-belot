//CREDIT
let Card = (function () {
    const DEFAULT_ROWS = 36; //every row is 3px, so default cardRows of the card will be 3 * 36 px
    const CARD_VALUES = {
        SEVEN: 7,
        EIGHT: 8,
        NINE: 9,
        TEN: 10,
        JACK: 'J',
        QUEEN: 'Q',
        KING: 'K',
        ACE: 'A',
        none: " "
    };
    const CARD_SUITS = {
        spades: '&#9824;',
        hearts: '&#9829;',
        clubs: '&#9827;',
        diamonds: '&#9830;',
        none: " "
    };
    const CARD_SUITS_COLORS = {
        spades: '#212121',
        hearts: '#d50000',
        clubs: '#2E2E2E',
        diamonds: '#e65100',
        none: "#CCC"
    };

    let cardRows = DEFAULT_ROWS;

    function isLowGraphics() {
        return document.getElementById("graphicsSwitch").checked;
    }

    function drawFaceDownCard() {
        return drawCard(" ", " ");
    }

    function drawCard(cardSuit, cardValue) {

        if (isLowGraphics()) {
            let card = document.createElement('div');
            card.className = 'belot-card';
            card.className += ' low-graph';
            let cardSuitKey =
                Object.keys(CARD_SUITS).find(key => CARD_SUITS[key] === cardSuit);
            card.style.color = CARD_SUITS_COLORS[cardSuitKey];
            for (let i = 0; i < cardRows; i++) {
                let row = document.createElement('p');
                row.className = 'card-row';
                if (i === 0 || i === cardRows - 1) {
                    row.innerHTML = cardValue;
                    row.classList.add(i === 0 ? "first-row" : "last-row");
                }
                if (i === Math.floor(cardRows / 4) - 1 || i === Math.ceil(cardRows / 2 + cardRows / 4)) {

                    row.innerHTML = `<p>${cardSuit}</p>` + `<p>${cardSuit}</p>`;
                    row.classList.add("suits-two");
                }

                if (i === cardRows / 2) {
                    row.innerHTML = cardSuit;
                    row.classList.add("middle-row");
                }

                card.appendChild(row);
            }
            return card;
        } else {
            let card = document.createElement('img');

            card.style.height = cardRows * 3 + "px";
            if (cardSuit === ' ') {
                card.src = "/sprites/bicycle-background.webp";
            } else {
                card.src = `/sprites/card-faces/${cardValue}-${cardSuit.toUpperCase()}.png`;
            }
            card.className = 'belot-card';
            card.className += ' high-graph';
            return card;
        }

    }

    function dealHand(selector, cards) {
        let baseCardOffset = 55;

        $(selector + " .belot-card").remove();

        cards.forEach((card, index) => {
            card.style.right = baseCardOffset * index + "px";

            $(selector).append(card);
        });

        let cardWidth =
            window.getComputedStyle(
                document.getElementsByClassName("belot-card")[0])
                .getPropertyValue("width")
                .replace("px", "");
        cardWidth = parseInt(cardWidth, 10);

        let handInPixels =
            ((cards.length - 1) * (cardWidth - baseCardOffset))
            + cardWidth;

        $(selector).width(handInPixels);

    }

    function parseCards(cardsJson) {

        let parsedCards = [];

        JSON.parse(cardsJson)
            .forEach((card) => {
                let cardSuit;
                if (isLowGraphics()) {
                    cardSuit = CARD_SUITS[card.suit.toLowerCase()];
                } else
                    cardSuit = card.suit;
                let cardValue = CARD_VALUES[card.cardValue];

                let cardElement = drawCard(cardSuit, cardValue);
                //TODO: only available cards, only in play phase
                let cardObj = {suit: card.suit, cardValue: card.cardValue};
                cardElement.addEventListener('click', () => {
                    let data = {type: "PLAY_CARD",
                        payload: JSON.stringify({
                            card : cardObj,
                            declarations : []
                        })
                    };
                    loggedInUser.gameSocket.sendMessage(data, "/belot/" + loggedInUser.belotId)
                });
                parsedCards.push(cardElement);
            });


        return parsedCards;
    }

    function renderFaceDownCards(selector, cardSize, isRotated) {
        let cards = [];
        for (let i = 0; i < cardSize; i++) {
            cards[i] = drawFaceDownCard();
        }
        dealHand(selector, cards, isRotated);
    }

    return {
        setHeight: (newHeight) => cardRows = newHeight,
        parseCards: (cardsJson) => parseCards(cardsJson),
        renderHand: (selector, cards) => dealHand(selector, cards),
        renderFaceDownCards: renderFaceDownCards
    }
})();
