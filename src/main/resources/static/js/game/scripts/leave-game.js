/**
 * Sends a message to the backend that the user is about to leave a game
 * It can accept a button(from html template) or a game id
 * @param btn the button invoking the action or the game id
 * TODO: This needs reworking with the backend too i suggest not writing anything here for now
 */
function leaveGame(btn) {
    let gameId = btn;
    if (btn instanceof HTMLButtonElement)
        gameId = btn.dataset.game;
    let message = {
        type: 'LEAVE_GAME'
    };


    gameSocket.sendMessage(message, "/belot/" + gameId);
    if (gameSocket.isConnected) {
        gameSocket.unsubscribeAll();
        gameSocket.disconnect();
    }
    showInitialGameScreen();
}