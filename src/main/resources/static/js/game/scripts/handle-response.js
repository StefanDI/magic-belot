/**
 * Handles websocket response for belot game
 * Expects SocketServerMessage with Header and message
 * the header is the type of server message {@see Header enum in java for more info}
 * the message is the payload of the message, typically in JSON format
 *
 * TODO: When adding new functionality in the back end, appropriate response handling should be implemented
 *
 * @type {Function} executed as callback to the {@link WebSocket#subscribe}
 */
let handleResponse = (function () {
    //If you are not familiar with js's IIFE, everything from here, until the return statement is private for this 'class' (object, function call it whatever)

    let isLastMessageSuccess;
    //This is the endpoint for sending messages to the backend
    const BASE_BELOT_ENDPOINT = "/belot";

    function handleAnnounce(message) {
        message = JSON.parse(message);
        let announces = message.availableAnnounces;
        let magicCards = message.availableMagicCards;

        if (announces) {
            Announce.createAnnounces(".game-wrapper", announces);
            renderer.resizeField("currentView");
        }
    }

    function drawCards(message) {
        let cards = Card.parseCards(message);

        Card.renderHand(".players .you", cards);
        Card.renderFaceDownCards(".players .partner", cards.length);
        Card.renderFaceDownCards(".players .left", cards.length);
        Card.renderFaceDownCards(".players .right", cards.length);

        renderer.resizeField("currentView");
    }

    function showReadyCheckModal(message) {
        loggedInUser.belotId = message;

        $("#foundRanked").on('show.bs.modal', () => {
            $("#searchRankedModal").modal('hide');
            $("#minimizedGameSearch")[0].style.display = 'none';
        });
        setTimeout(() => {
            $("#foundRanked").modal({
                backdrop: false,
                keyboard: false,
                show: true
            });
        }, 100);
    }

    function renderPlayers(message) {
        message = JSON.parse(message);
        let left = message["left"];
        let right = message["right"];
        let partner = message["partner"];

        $(".players .left .username").text(left.username);
        $(".players .right .username").text(right.username);
        $(".players .partner .username").text(partner.username);
        $(".players .you .username").text(loggedInUser.username);

    }

    function subscribeTo(belotId) {
        {
            loggedInUser.belotId = belotId;
            loggedInUser.gameSocket.subscribe("/user/topic/belot-game/" + loggedInUser.belotId, handleResponse);
            loggedInUser.gameSocket.subscribe("/topic/belot-game/" + loggedInUser.belotId, handleResponse);
        }
    }

//If you are not familiar with js's IIFE, everything from here is the public part for this 'class' (object, function call it whatever)
    return function (resp) {
        //Parse the response body
        let body = JSON.parse(resp.body);
        let header = body.header;
        let message = body.message;

        //Different behavior for different headers
        switch (header) {
            case "ERROR" : {
                loggedInUser.messageSend = "ERROR";
                loggedInUser.messageData = message;
            }
                break;
            case "SUCCESS" : {
                loggedInUser.messageSend = "SUCCESS";
                loggedInUser.messageData = message;
            }
                break;
            case "SUBSCRIBE_TO" : subscribeTo(message);
                break;
            //Game is actually ABOUT TO start, send "READY" message to backend
            case "READY_CHECK" :
                showReadyCheckModal(message);
                break;
            case "CARDS" :
                drawCards(message);
                break;
            case "ANNOUNCE":
                handleAnnounce(message);
                break;
            case "PLAYERS_INFO":
                renderPlayers(message);
                break;
            case "PLAYER_ACTION": {
                console.log(message);
                message = JSON.parse(message);
                let actions = message["turnInfo"];
                let user = message["username"];
                Array.from(document.getElementsByClassName("username")).forEach(el => {
                    if(el.innerHTML === user)
                        el.innerHTML = el.innerHTML + " " + actions["ANNOUNCE"]
                });
                for (let action in actions) {
                    //TODO: render
                    if (actions.hasOwnProperty(action))
                        switch (action) {
                            case "MAGIC_CARD_PLAYED" : {
                            }
                                break;
                            case "ANNOUNCE" : {
                            }
                                break;
                            case "CARD_PLAYED": {
                            }
                                break;
                        }
                }
            }
                break;
            case "YOUR_TURN" : {
                drawCards(message);

            }
            case "PLAYER_LEFT" : {
            }
                break;
        }

    }
})();