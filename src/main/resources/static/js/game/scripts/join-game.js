/**
 * @type {Function}
 */
let joinGame = (function f() {

    return function () {
        showGameView().then(() => {
            loggedInUser.gameSocket.sendMessage({type: "JOIN_PRIVATE_GAME"}, "/belot/");
        });
    }
})();
