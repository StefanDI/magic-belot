/**
 * Snackbar is a material component for a popup window, its useful for
 * not very important messages, like success messages, info messages,
 * it can even have an 'action' button for some minor action
 * @link https://material.io/components/snackbars/#usage
 *
 * @type {Object}
 */
const SnackBar = (function () {
    // Any snackbar that is already shown
    let previous = null;

    /* This is the snackbar in html
    <div class="paper-snackbar">
      <button class="action">Dismiss</button>
      This is a longer message that won't fit on one line. It is,
       inevitably, quite a boring thing. Hopefully it is still useful.
    </div>
    */


    return {
        /**
         * Creates a snackbar, message is the only required argument
         * (tho you can pass null it will just render empty snackbar)
         * @param message      the message of the snackbar
         * @param actionText   the 'action' button text (optional)
         * @param action       the action invoked by the action button (optional)
         * NOTE if actionText is present, but action is not,
         * the action button will dismiss the snackbar
         * @param dismissAfter automatically dismiss the snackbar after this time (optional, default is 2s)
         */
        createSnackbar: function (message, actionText, action, dismissAfter) {
            if (previous) {
                previous.dismiss();
            }
            let snackbar = document.createElement('div');
            snackbar.className = 'paper-snackbar';
            snackbar.dismiss = function () {
                this.style.opacity = 0;
            };
            let text = document.createTextNode(message);
            snackbar.appendChild(text);
            if (actionText) {
                if (!action) {
                    action = snackbar.dismiss.bind(snackbar);
                }
                let actionButton = document.createElement('button');
                actionButton.className = 'action';
                actionButton.innerHTML = actionText;
                actionButton.addEventListener('click', action);
                snackbar.appendChild(actionButton);
            }

            let dismissTime = dismissAfter || Constants.DEFAULT_SNACKBAR_DISMISS_TIME;
            //If the dismiss time is more than 20 seconds, never dismiss it
            if (dismissTime < 20000)
                setTimeout(function () {
                    if (previous === this) {
                        previous.dismiss();
                    }
                }.bind(snackbar), dismissTime);

            snackbar.addEventListener('transitionend', function (event, elapsed) {
                if (event.propertyName === 'opacity' && this.style.opacity === '0') {
                    this.parentElement.removeChild(this);
                    if (previous === this) {
                        previous = null;
                    }
                }
            }.bind(snackbar));


            previous = snackbar;
            document.body.appendChild(snackbar);

            // In order for the animations to trigger, I have to force the original style to be computed, and then change it.
            getComputedStyle(snackbar).bottom;
            snackbar.style.bottom = '0px';
            snackbar.style.opacity = 1;
        },
        /**
         * If we want to show a snackbar after we submit a form or reload a page
         * we can schedule it to be rendered after the page refreshes
         * //TODO: if needed make it accept actionText and onActionClicked function too
         * @param snackbarMessage the message
         */
        scheduleSnackbarAfterReload: function (snackbarMessage) {
            localStorage.setItem("snackbar", "true");
            localStorage.setItem("snackbar-message", snackbarMessage);
        }
    }
})();
