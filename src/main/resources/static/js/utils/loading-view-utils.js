/**
 * contains necessary functions for loading animation
 * every html element that you want to insert loading into MUST have class .loading-screen-container
 *
 * @type {Object}
 */
const Loading = (function() {
    return {
        /**
         * Creates a loading element with a given id
         * always pass id
         * @param id the id of the loaders top div
         * @returns {string} the loading html
         */
        getLoading: function (id) {
            return `<div id="${id}" class=\"loading-background\">\n` +
                "    <div class=\"loading-wrapper\">\n" +
                "        <div class=\"loading-inner\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>"
        },
        /**
         * appends a loading circle with a given loadingId to the given selector
         * if selector isn't passed, loads to default selector
         * NOTE: DO NOT use it without selector, for default selector is a class
         * that every loading container should contain the default class (see top jsdoc)
         * and it will insert loading into EVERY div with that class
         *
         * @param loadingId the id of the loading component
         * @param selector the selector of the element to be insert
         *
         */
        startLoading: function (loadingId, selector) {
            if (!selector)
                selector = Constants.DEFAULT_LOADING_CONTAINER_SELECTOR;

            $(selector).append(this.getLoading(loadingId));
        },
        /**
         * removes a loading by id (given in the {@link startLoading} or {@link getLoading} functions
         * @param loadingId the id of the loading component to be removed
         */
        stopLoading: function (loadingId) {
            $("#" + loadingId).remove();
        }
    }
})();
