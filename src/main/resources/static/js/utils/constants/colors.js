let AppTheme = (function () {
    let currentTheme = "default";
    let colorMap = {
        "default": {
            primary: "#4caf50",//Secondary? 03a9f4
            "primary-dark": "#1b5e20",
            "primary-light": "#A5D6A7",
            secondary: "#ffc107",
            "secondary-accent": "#ffd54f",
            accent: "#69F0AE",
            background: "#EEE",
            text: "rgba(0,0,0,0.80)",
            "text-secondary": "rgba(0,0,0,0.57)",
            divider: "rgba(0,0,0,.12)",
            shadow: "#777",
            hover: "#CC3363",
            "color-error": "#f44336",
            "color-success": "#00c851",
            "color-input-underline": "#ced4da",
            "default-icon-color": "#000000",
            disabled:"#cfcfcf"
        },
        "dark": {
            primary: "#2e2e2e",
            "primary-dark": "#212121",
            "primary-light": "#BCAAA4",
            secondary: "#37474F",
            "secondary-accent": "#607d8b",
            accent: "#3F729B",
            background: "#4B515D",
            text: "rgba(255,255,255,0.80)",
            "text-secondary": "rgba(255,255,255,0.57)",
            divider: "rgba(255,255,255,.12)",
            shadow: "#7d8491",
            hover: "#9933CC",
            "color-error": "#f44336",
            "color-success": "#00c851",
            "color-input-underline": "#ced4da",
            "default-icon-color": "#000000",
            disabled:"#ddd"
        }
    };

    return {
        setTheme: function (theme) {
            if (theme === "default" || theme === "dark")
                currentTheme = theme;
            else
                throw "Invalid theme";
        },
        getPrimary: () => colorMap[currentTheme].primary,
        getSecondary: () => colorMap[currentTheme].secondary,
        getAccent: () => colorMap[currentTheme].accent,
        getSecondaryAccent: () => colorMap[currentTheme]["secondary-accent"],
        getPrimaryDark: () => colorMap[currentTheme]["primary-dark"],
        getPrimaryLight: () => colorMap[currentTheme]["primary-light"],
        getBackground: () => colorMap[currentTheme].background,
        getDivider: () => colorMap[currentTheme].divider,
        getText: () => colorMap[currentTheme].text,
        getShadow: () => colorMap[currentTheme].shadow,
        getHover: () => colorMap[currentTheme].accent,
        getSuccess: () => colorMap[currentTheme]["color-success"],
        getError: () => colorMap[currentTheme]["color-error"],
        getInputBorder: () => colorMap[currentTheme]["color-input-underline"],
        getIconColor: () => colorMap[currentTheme]["default-icon-color"],
        getDisabled: () => colorMap[currentTheme]["disabled"],
        applyTheme: function () {
            for (let anchor of document.getElementsByTagName("a")) {
                anchor.onmouseover = function () {
                    this.style.color = AppTheme.getHover();
                };
                anchor.onmouseout = function () {
                    this.style.color = AppTheme.getText();
                };
                anchor.style.color = AppTheme.getText();
            }
            for (let label of document.getElementsByTagName("label")) {
                    label.style.color = AppTheme.getText();
            }
            for(let disabledLabel of document.querySelectorAll("input[disabled]:not([readonly]) + label")){
                disabledLabel.style.color = AppTheme.getDisabled();
            }
            for (let backgroundElement of document.getElementsByClassName("app-background")) {
                backgroundElement.style.background = AppTheme.getBackground();
            }
            for (let primaryBgElement of document.getElementsByClassName("app-primary")) {
                primaryBgElement.style.background = AppTheme.getPrimary();
            }
            for (let primaryLightBgElement of document.getElementsByClassName("app-primary-light")) {
                primaryLightBgElement.style.background = AppTheme.getPrimaryLight();
            }
            for (let textBgElement of document.getElementsByClassName("app-text")) {
                textBgElement.style.color = AppTheme.getText();
            }
            for (let secondaryBgElement of document.getElementsByClassName("app-secondary")) {
                secondaryBgElement.style.background = AppTheme.getSecondary();
            }
            for (let primaryDarkBgElement of document.getElementsByClassName("app-primary-dark")) {
                primaryDarkBgElement.style.background = AppTheme.getPrimaryDark();
            }
            for (let accentElements of document.getElementsByClassName("app-accent")) {
                accentElements.style.background = AppTheme.getAccent();
            }
            for (let secondaryElement of document.getElementsByClassName("app-secondary-accent")) {
                secondaryElement.style.background = AppTheme.getSecondaryAccent();
            }
            for (let textSecondaryEl of document.getElementsByClassName("app-text-secondary")) {
                textSecondaryEl.style.color = AppTheme.getSecondaryAccent();
            }
            for (let divider of document.getElementsByClassName("app-divider")) {
                divider.style["border-color"] = AppTheme.getDivider();
            }
            for (let shadowElement of document.getElementsByClassName("has-shadow")) {
                let shadowProperty =
                    window.getComputedStyle(shadowElement).getPropertyValue("box-shadow") === ""
                        ?
                        "textShadow" : "boxShadow";
                let oldShadow = shadowProperty === "boxShadow" ?
                    window.getComputedStyle(shadowElement).getPropertyValue("box-shadow")
                    :
                    window.getComputedStyle(shadowElement).getPropertyValue("text-shadow");

                shadowElement.style[shadowProperty] =
                    oldShadow.replace(/rgb\(.*\)/, AppTheme.getShadow());
            }
            for (let icon of document.getElementsByTagName("i")) {
                icon.style.color = AppTheme.getIconColor();
            }
            for (let btnPrimary of document.getElementsByClassName("app-btn-primary")) {
                btnPrimary.onmouseover = function () {
                    this.style.backgroundColor = AppTheme.getPrimary() + "CD";
                };
                btnPrimary.onmouseout = function () {
                    btnPrimary.style.backgroundColor = AppTheme.getPrimary();
                };
                btnPrimary.style.backgroundColor = AppTheme.getPrimary();
                btnPrimary.style.color = AppTheme.getText();
            }
            for (let btnPrimaryOutline of document.getElementsByClassName("app-btn-primary-outline")) {
                btnPrimaryOutline.style.backgroundColor = AppTheme.getBackground();
                btnPrimaryOutline.style.color = AppTheme.getPrimary();
            }
            for (let btnSecondary of document.getElementsByClassName("app-btn-secondary")) {
                btnSecondary.onmouseover = function () {
                    this.style.backgroundColor = AppTheme.getSecondary() + "CD";
                };
                btnSecondary.onmouseout = function () {
                    btnSecondary.style.backgroundColor = AppTheme.getSecondary();
                };
                btnSecondary.style.backgroundColor = AppTheme.getSecondary();
                btnSecondary.style.color = AppTheme.getText();
            }
            for (let btnSecondaryOutlined of document.getElementsByClassName("app-btn-primary-outline")) {
                btnSecondaryOutlined.style.backgroundColor = AppTheme.getBackground();
                btnSecondaryOutlined.style.color = AppTheme.getSecondary();
            }

            $("input:not([readonly])").each(function () {
                this.style.color = AppTheme.getText();
                this.addEventListener("focus", () => {

                    if (this.previousElementSibling && this.previousElementSibling.tagName.toLowerCase() === 'i')
                        this.previousElementSibling.style.color = AppTheme.getAccent();
                    this.style["border-bottom"] = "1px solid" + AppTheme.getAccent();
                    this.style["box-shadow"] = "0 1px 0 0 " + AppTheme.getAccent();

                });
                this.addEventListener("focusout", () => {
                    let hasIcon = this.previousElementSibling && this.previousElementSibling.tagName.toLowerCase() === 'i';
                    let icon = this.previousElementSibling;

                    if (this.classList.contains("invalid")) {
                        if (hasIcon)
                            icon.style.color = AppTheme.getError();
                        this.style["border-bottom"] = `1px solid ${AppTheme.getError()}`;
                        this.style["box-shadow"] = "none";
                        this.style.color = AppTheme.getError();
                    } else if (this.classList.contains("valid")) {
                        if (hasIcon)
                            icon.style.color = AppTheme.getSuccess();
                        this.style["border-bottom"] = `1px solid ${AppTheme.getSuccess()}`;
                        this.style["box-shadow"] = "none";
                        this.style.color = AppTheme.getSuccess();
                    } else {
                        if (hasIcon)
                            icon.style.color = AppTheme.getIconColor();
                        this.style["border-bottom"] = `1px solid ${AppTheme.getInputBorder()}`;
                        this.style["box-shadow"] = "none";
                        this.style.color = AppTheme.getText();
                    }
                });
            });


        }
    }
})();