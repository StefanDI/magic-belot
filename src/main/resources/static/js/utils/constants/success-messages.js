/**
 * I have an ambition all messages to be pulled in a constants class
 * so if one day we decide to translate, we can hold the translations here
 * TODO: Maybe make all messages in one object and not divide them as success-messages and error-messages
 * TODO: make socket messages as constants too (i suggest leaving this TODO for me (stefan))
 * TODO: No hard coded strings in the client side, pull everything here
 * @type {Object}
 */
const SuccessMessages = (function () {
   return {
       EMAIL_NOT_TAKEN: "Email available",
       USERNAME_NOT_TAKEN: "Username available",
       REGISTER_SUCCESSFUL : "Registered successfully",
       LOGIN_SUCCESSFUL : "Logged in successfully",
       LOGOUT_SUCCESSFUL: "Logged out successfully"

   }
})();