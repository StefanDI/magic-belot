const Constants = (function () {
   return {
       DEFAULT_LOADING_CONTAINER_SELECTOR : ".loading-screen-container",
       DEFAULT_SNACKBAR_DISMISS_TIME: 2000
   }
})();