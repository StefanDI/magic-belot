/**
 * I have an ambition all messages to be pulled in a constants class
 * so if one day we decide to translate, we can hold the translations here
 * TODO: Maybe make all messages in one object and not divide them as success-messages and error-messages
 * TODO: make socket messages as constants too (i suggest leaving this TODO for me (stefan))
 * TODO: No hard coded strings in the client side, pull everything here
 * @type {Object}
 */
let ErrorMessages = (function(){

    return {
        USERNAME_NOT_NULL : "Username must not be empty",
        USERNAME_TOO_SMALL : "Username must be at least 3 symbols long",
        USERNAME_TOO_BIG : "Username must be less than 20 symbols long",
        USERNAME_ILLEGAL_CHARACTERS : "Username contains illegal characters",
        INVALID_EMAIL : "Invalid email",
        PASSWORD_TOO_SHORT : "Password is too short, password must be at least 6 symbols",
        PASSWORDS_DONT_MATCH : "Passwords dont match",
        EMAIL_NOT_NULL: "Email cannot be empty",
        EMAIL_TAKEN: "Email taken",
        USERNAME_TAKEN: "Username taken"
    }

}());