/**
 * This is the websocket class, i decided to make it ES6 class, just for exercise,
 * it could very well be just another IIFE, for consistency
 * <br/>
 *    Handles base communication through websocket, with STOMP protocol
 * @see {@link https://stomp-js.github.io/guide/stompjs/rx-stomp/ng2-stompjs/2018/09/10/using-stomp-with-sockjs.html}
 * for SockJS (its basically legacy support for WebSockets)
 *
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/WebSocket}
 * for WebSockets info (SockJS uses this below the hood)
 *
 * @see {@link https://stomp.github.io/} for STOMP protocol (its quite similar to HTTP)
 *
 * NOTE: I realized i named this class the same as javascript's WebSocket class,
 * so be careful when initializing it or TODO: rename this
 *
 */
class WebSocket {

    /**
     * prepares the connection, but without initializing it
     * @param socketEndpoint this should be the url provided in
     * backend's com.isoft.belot.config.WebSocketConfig#registerStompEndpoints() method
     * In our case "/belot"
     */
    constructor(socketEndpoint) {
        // noinspection JSUnresolvedFunction
        let socket = new SockJS(socketEndpoint);

        this.stompClient = Stomp.over(socket);
        this.isConnected = false;
    }

    /**
     * Make the connection to the server
     * @param callback invoked when connection is complete
     */
    connect(callback) {
        this.stompClient.connect({}, () => {
            this.isConnected = true;
            callback();
        });
    }

    /**
     * disconnect the socket and execute the beforeDisconnect function if set via {@link setBeforeDisconnect}
     */
    disconnect() {
        if (this.stompClient !== null && this.isConnected) {
            this.isConnected = false;

            if (this.beforeDisconnect)
                this.beforeDisconnect();

            this.stompClient.disconnect();


            console.log("Disconnected");
        }
    }

    /**
     * Subscribe the socket to the provided topic
     * @param subscribeUrl   the topic to be subscribed to
     * @param messageHandler the function invoked when message is received on that topic
     *
     * TODO: pass an identifier as parameter to the subscription
     *  so we can later unsubscribeAll only to a given topic
     */
    subscribe(subscribeUrl, messageHandler) {
        return this.stompClient.subscribe(subscribeUrl, function (data) {
            messageHandler(data);
        });
    }

    /**
     * unsubscribeAll the client from the topic
     * when implementing this class i didnt know the client will subscribe to multiple topics
     * TODO: unsubscribeAll the given topic passed as parameter
     */
    unsubscribeAll() {
        Object.keys(this.stompClient.subscriptions).forEach(subscription => {
            this.stompClient.unsubscribe(subscription);
        });

    }

    unsubscribe(topic) {
        topic.unsubscribe();
        // this.stompClient.unsubscribe(topicId);
    }

    /**
     * Send a message to the provided url
     * the backend should have a @MessageMapping annotation
     * with the EXACT url provided
     *
     * 01.10.2019 Now returns a promise, with logic handled by {@link handleResponse}
     * @see BelotController for example
     * @param data  the data to send over the socket
     * @param url   the url
     */
    sendMessage(data, url) {
        this.stompClient.send(url, {}, JSON.stringify(data));
    }

    sendMessageWithPromise(data, url) {
        return new Promise((resolve, error) => {
            this.stompClient.send(url, {}, JSON.stringify(data));
            loggedInUser.messageSend = "SENT";
            loggedInUser.messageData = "";
            while (true) {
                if (loggedInUser.messageSend !== "SENT") {
                    if (loggedInUser.messageSend === "ERROR")
                        error(loggedInUser.messageData);
                    else
                        resolve(loggedInUser.messageData);
                    break;
                }
            }
        });
    }

    /**
     * Set a function to be executed before disconnecting the socket
     * @param callback the function
     */
    setBeforeDisconnect(callback) {
        this.beforeDisconnect = callback;
    }
}