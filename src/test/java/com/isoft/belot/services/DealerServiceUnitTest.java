package com.isoft.belot.services;

import com.google.common.collect.Iterables;
import com.isoft.belot.domain.entities.Announce;
import com.isoft.belot.domain.entities.Card;
import com.isoft.belot.domain.entities.Dealing;
import com.isoft.belot.domain.entities.Deck;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.CardValue;
import com.isoft.belot.domain.entities.enums.Declaration;
import com.isoft.belot.domain.entities.enums.Suit;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * @author Aleksandar Todorov
 */
public class DealerServiceUnitTest
{
    private final Dealing dealing = new Dealing();
    private final Set<Announce> announces = new LinkedHashSet<>();
    private final Announce announce1 = new Announce();
    private final Announce announce2 = new Announce();
    private final Announce announce3 = new Announce();
    private final Announce announce4 = new Announce();
    private final Announce announce5 = new Announce();
    private final Announce announce6 = new Announce();
    private DealerServiceImpl dealerServiceImpl;
    private Set<Card> cards;

    @Before
    public void setUp()
    {
        dealerServiceImpl = new DealerServiceImpl();
        announce1.setAnnounceType(AnnounceType.CLUBS);
        announce1.setCreated(LocalDateTime.now().minusMinutes(6));
        announce2.setAnnounceType(AnnounceType.DOUBLE);
        announce2.setCreated(LocalDateTime.now().minusMinutes(5));
        announce3.setAnnounceType(AnnounceType.RE_DOUBLE);
        announce3.setCreated(LocalDateTime.now().minusMinutes(4));
        announce4.setAnnounceType(AnnounceType.PASS);
        announce4.setCreated(LocalDateTime.now().minusMinutes(3));
        announce5.setAnnounceType(AnnounceType.PASS);
        announce5.setCreated(LocalDateTime.now().minusMinutes(2));
        announce6.setAnnounceType(AnnounceType.PASS);
        announce6.setCreated(LocalDateTime.now().minusMinutes(1));
        announces.add(announce1);
        announces.add(announce2);
        announces.add(announce3);
        announces.add(announce4);
        announces.add(announce5);
        announces.add(announce6);
        dealing.setAnnounces(announces);

        cards = new HashSet<>(new Deck().getCards());
    }

    @Test
    public void DealingService_method_determineCurrentAnnounceType_worksCorrectly_withREDOUBLE()
    {
        assertEquals(AnnounceType.CLUBS, dealerServiceImpl.determineRoundAnnounceType(dealing));
    }

    @Test
    public void DealingService_method_determineCurrentAnnounceType_worksCorrectly_withDOUBLE()
    {
        dealing.getAnnounces().remove(announce3);
        assertEquals(AnnounceType.CLUBS, dealerServiceImpl.determineRoundAnnounceType(dealing));
    }

    @Test
    public void DealingService_method_determineCurrentAnnounceType_worksCorrectly_withoutDOUBLEANDREDOUBLE()
    {
        dealing.getAnnounces().remove(announce2);
        dealing.getAnnounces().remove(announce3);
        assertEquals(AnnounceType.CLUBS, dealerServiceImpl.determineRoundAnnounceType(dealing));
    }

    @Test
    public void DealingService_method_endAnnouncesPhase_returnTrue_with3PASS()
    {
        assertTrue(dealing.twoConsecutivePass());
    }

    @Test
    public void DealingService_method_endAnnouncesPhase_returnTrue_with2PASS()
    {
        dealing.getAnnounces().remove(announce6);
        assertTrue(dealing.twoConsecutivePass());
    }

    @Test
    public void DealingService_method_endAnnouncesPhase_returnFalse_with1PASS()
    {
        dealing.getAnnounces().remove(announce5);
        dealing.getAnnounces().remove(announce6);
        assertFalse(dealing.twoConsecutivePass());
    }

    @Test
    public void DealingService_method_endAnnouncesPhase_returnFalse_withOnlyTwoElements()
    {
        dealing.getAnnounces().remove(announce1);
        dealing.getAnnounces().remove(announce2);
        dealing.getAnnounces().remove(announce3);
        dealing.getAnnounces().remove(announce4);
        assertFalse(dealing.twoConsecutivePass());
    }

    @Test
    public void DealingService_method_determinePointsMultiplier_return4_withREDOUBLE()
    {
        assertEquals(4, dealerServiceImpl.determinePointsMultiplier(dealing));
    }

    @Test
    public void DealingService_method_determinePointsMultiplier_return2_withDOUBLE()
    {
        dealing.getAnnounces().remove(announce3);
        assertEquals(2, dealerServiceImpl.determinePointsMultiplier(dealing));
    }

    @Test
    public void DealingService_method_determinePointsMultiplier_return1_withoutDOUBLEandReDOUBLE()
    {
        dealing.getAnnounces().remove(announce2);
        dealing.getAnnounces().remove(announce3);
        assertEquals(1, dealerServiceImpl.determinePointsMultiplier(dealing));
    }

    @Test
    public void DealingService_method_whoseCardWins_withAnnounceType_ALLTRUMPS()
    {

        Card actual = dealerServiceImpl.whichCardWins(cards, AnnounceType.ALL_TRUMPS);

        assertEquals(Iterables.getFirst(cards, null).getSuit(), actual.getSuit());
        assertEquals(CardValue.JACK, actual.getCardValue());
    }

    @Test
    public void DealingService_method_whoseCardWins_withAnnounceType_NOTRUMPS()
    {
        Set<Card> cards = new HashSet<>(new Deck().getCards());

        Card actual = dealerServiceImpl.whichCardWins(cards, AnnounceType.NO_TRUMPS);

        assertEquals(Iterables.getFirst(cards, null).getSuit(), actual.getSuit());
        assertEquals(CardValue.ACE, actual.getCardValue());
    }

    @Test
    public void DealingService_method_whoseCardWins_withAnnounceType_CLUBS_WITHCLUBS()
    {
        Card actual = dealerServiceImpl.whichCardWins(cards, AnnounceType.CLUBS);

        assertEquals(Suit.CLUBS, actual.getSuit());
        assertEquals(CardValue.JACK, actual.getCardValue());
    }

    @Test
    public void DealingService_method_whoseCardWins_withAnnounceType_CLUBS_WITHOUTCLUBS()
    {
        cards = cards.stream().filter(card -> !card.getSuit().equals(Suit.CLUBS)).collect(Collectors.toSet());

        Card actual = dealerServiceImpl.whichCardWins(cards, AnnounceType.CLUBS);

        assertEquals(Iterables.getFirst(cards, null).getSuit(), actual.getSuit());
        assertEquals(CardValue.ACE, actual.getCardValue());
    }

    @Test
    public void DealingService_method_whoseCardWins_withAnnounceType_DIAMONDS_WITHDIAMONDS()
    {
        Card actual = dealerServiceImpl.whichCardWins(cards, AnnounceType.DIAMONDS);

        assertEquals(Suit.DIAMONDS, actual.getSuit());
        assertEquals(CardValue.JACK, actual.getCardValue());
    }

    @Test
    public void DealingService_method_whoseCardWins_withAnnounceType_DIAMONDS_WITHOUTDIAMONDS()
    {
        cards = cards.stream().filter(card -> !card.getSuit().equals(Suit.DIAMONDS)).collect(Collectors.toSet());

        Card actual = dealerServiceImpl.whichCardWins(cards, AnnounceType.DIAMONDS);

        assertEquals(Iterables.getFirst(cards, null).getSuit(), actual.getSuit());
        assertEquals(CardValue.ACE, actual.getCardValue());
    }

    @Test
    public void calculateDealingPoints_method_works_properly_with_one_declaration()
    {
        List<Declaration> declarations = new ArrayList<>();
        declarations.add(Declaration.CHAIN_OF_4);

        assertEquals(212, dealerServiceImpl.calculateDealingPoints(AnnounceType.CLUBS, declarations));
    }

    @Test
    public void calculateDealingPoints_method_works_properly_with_two_declaration()
    {
        List<Declaration> declarations = new ArrayList<>();
        declarations.add(Declaration.CHAIN_OF_4);
        declarations.add(Declaration.CHAIN_OF_5_PLUS);

        assertEquals(312, dealerServiceImpl.calculateDealingPoints(AnnounceType.CLUBS, declarations));
    }

    @Test
    public void calculateDealingPoints_method_get_CardValue_properly()
    {
        List<Declaration> declarations = new ArrayList<>();

        Declaration declaration = Declaration.CHAIN_OF_4;
        declaration.setCardValue(CardValue.KING);
        declarations.add(declaration);

        assertEquals(212, dealerServiceImpl.calculateDealingPoints(AnnounceType.CLUBS, declarations));
        assertEquals(CardValue.KING, declarations.get(0).getCardValue());
    }
}