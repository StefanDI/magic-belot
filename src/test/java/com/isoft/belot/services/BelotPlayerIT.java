package com.isoft.belot.services;

import com.isoft.belot.domain.entities.Belot;
import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.User;
import com.isoft.belot.repository.BelotPlayerRepository;
import com.isoft.belot.repository.UserRepository;
import com.isoft.belot.services.interfaces.BelotPlayerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * testing belotPlayerService methods : update
 * @author Aleksandar Todorov
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class BelotPlayerIT
{
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BelotPlayerRepository belotPlayerRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void belotPlayerService_saveResultWithCorrectValues_ReturnsCorrect()
    {
        BelotPlayerService belotPlayerService = new BelotPlayerServiceImpl(belotPlayerRepository, userRepository);

        //given
        User user = new User();
        user.setUsername("user");
        user.setEmail("user@mail.bg");
        user.setPassword("trudnaparola");
        entityManager.persistAndFlush(user);

        Belot belot = new Belot();
        entityManager.persistAndFlush(belot);

        BelotPlayer belotPlayer = new BelotPlayer(user, belot);
        belotPlayerService.update(belotPlayer);

        //when
        BelotPlayer expected = belotPlayerRepository.findByUser(user);

        //then
        assertThat(expected.getUser().getUsername()).isEqualTo(user.getUsername());
        assertThat(expected.getUser().getPassword()).isEqualTo(user.getPassword());
        assertThat(expected.getUser().getEmail()).isEqualTo(user.getEmail());
    }
}
