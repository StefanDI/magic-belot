package com.isoft.belot.services;

import com.isoft.belot.domain.entities.User;
import com.isoft.belot.game.communication.client.SocketClientMessage;
import com.isoft.belot.game.communication.client.enums.ClientMessageType;
import com.isoft.belot.game.communication.server.SocketServerMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.RestTemplateXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.isoft.belot.util.DummyUsersGenerator.emails;
import static com.isoft.belot.util.DummyUsersGenerator.names;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertNotNull;

/**
 * @author Stefan Ivanov
 */

//@SpringApplicationConfiguration({WebSocketConfig.class})
//@WebIntegrationTest(value = 8080)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameMatchingServiceTest
{
    @Value("${local.server.port}")
    private int port;
    private String URL;
    private User[] users = new User[4];
    private SocketClientMessage clientMessage;

    private static final String SUBSCRIBE_FOR_BELOT_URL = "/topic/belot-game";
    private static final String SUBSCRIBE_FOR_BELOT_USER_URL = "/user/topic/belot-game";

    private CompletableFuture<SocketServerMessage> completableFuture;

    @Before
    public void setup()
    {
        clientMessage = new SocketClientMessage();
        for (int i = 0; i < 4; i++)
        {
            User u = new User();
            u.setUsername(names[i]);
            u.setEmail(emails[i]);
            u.setPassword("password");
            users[i] = u;
        }
        completableFuture = new CompletableFuture<>();
        URL = "ws://localhost:" + port + "/belot/";
    }


    @Test
    public void testCreateGameEndpoint() throws URISyntaxException, InterruptedException, ExecutionException, TimeoutException
    {
//        String uuid = UUID.randomUUID().toString();

//        SockJsClient sockJsClient =;
//        sockJsClient.doHandshake(getMyWebSocketHandler(), URL);
        WebSocketStompClient stompClient = new WebSocketStompClient( new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter()
        {
        }).get(1, SECONDS);

        stompClient.start();

        stompSession.subscribe("ws://localhost:" + port + SUBSCRIBE_FOR_BELOT_URL, new CreateGameStompFrameHandler());
        stompSession.subscribe("ws://localhost:" + port + SUBSCRIBE_FOR_BELOT_USER_URL, new CreateGameStompFrameHandler());
        clientMessage = new SocketClientMessage();
        clientMessage.setSender(users[0]);
        clientMessage.setType(ClientMessageType.JOIN_PRIVATE_GAME);
        clientMessage.setPayload("Hello world");
        stompSession.send(URL, clientMessage);
        Object o = completableFuture.get(15, SECONDS);
        System.out.println(o);
        for (int i = 0; i < 4; i++)
        {
            clientMessage = new SocketClientMessage();
            clientMessage.setSender(users[i]);
            clientMessage.setType(ClientMessageType.SEARCH_RANKED);
            stompSession.send(URL, clientMessage);
        }

        SocketServerMessage gameState = completableFuture.get(10, SECONDS);

        assertNotNull(gameState);
    }

    private WebSocketHandler getMyWebSocketHandler()
    {
        return new WebSocketHandler()
        {
            @Override
            public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception
            {
                System.out.println(webSocketSession);
            }

            @Override
            public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception
            {
                System.out.println("message:  " + webSocketMessage);
            }

            @Override
            public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception
            {
                throwable.printStackTrace();
            }

            @Override
            public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception
            {

            }

            @Override
            public boolean supportsPartialMessages()
            {
                return true;
            }
        };
    }


    private List<Transport> createTransportClient()
    {
        List<Transport> transports = new ArrayList<>(2);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        transports.add(new RestTemplateXhrTransport());

        return transports;
    }

    private class CreateGameStompFrameHandler implements StompFrameHandler
    {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders)
        {
            System.out.println("called");
            return SocketServerMessage.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o)
        {
            System.out.println(o);
            completableFuture.complete((SocketServerMessage) o);
        }
    }
}

