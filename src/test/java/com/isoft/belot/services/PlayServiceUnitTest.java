package com.isoft.belot.services;

import com.isoft.belot.domain.entities.Card;
import com.isoft.belot.domain.entities.enums.AnnounceType;
import com.isoft.belot.domain.entities.enums.CardValue;
import com.isoft.belot.domain.entities.enums.Suit;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * @author Aleksandar Todorov
 */
public class PlayServiceUnitTest
{
    private DealerServiceImpl dealerServiceImpl = new DealerServiceImpl();
    private Card card1;
    private Card card2;
    private Card card3;
    private Card card4;

    @Before
    public void setUp()
    {
        card1 = new Card(Suit.CLUBS, CardValue.TEN);
        card2 = new Card(Suit.CLUBS, CardValue.JACK);
        card3 = new Card(Suit.DIAMONDS, CardValue.TEN);
        card4 = new Card(Suit.DIAMONDS, CardValue.ACE);
    }

    @Test
    public void whichCardWins_method_working_properly()
    {
        Set<Card> cards = new LinkedHashSet<>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);
        cards.add(card4);

        assertEquals(Suit.CLUBS, dealerServiceImpl.whichCardWins(cards, AnnounceType.CLUBS).getSuit());
        assertEquals(CardValue.JACK, dealerServiceImpl.whichCardWins(cards, AnnounceType.CLUBS).getCardValue());

        assertEquals(Suit.DIAMONDS, dealerServiceImpl.whichCardWins(cards, AnnounceType.DIAMONDS).getSuit());
        assertEquals(CardValue.ACE, dealerServiceImpl.whichCardWins(cards, AnnounceType.DIAMONDS).getCardValue());

        assertEquals(Suit.CLUBS, dealerServiceImpl.whichCardWins(cards, AnnounceType.HEARTS).getSuit());
        assertEquals(CardValue.TEN, dealerServiceImpl.whichCardWins(cards, AnnounceType.HEARTS).getCardValue());

        assertEquals(Suit.CLUBS, dealerServiceImpl.whichCardWins(cards, AnnounceType.NO_TRUMPS).getSuit());
        assertEquals(CardValue.TEN, dealerServiceImpl.whichCardWins(cards, AnnounceType.NO_TRUMPS).getCardValue());

        assertEquals(Suit.CLUBS, dealerServiceImpl.whichCardWins(cards, AnnounceType.ALL_TRUMPS).getSuit());
        assertEquals(CardValue.JACK, dealerServiceImpl.whichCardWins(cards, AnnounceType.ALL_TRUMPS).getCardValue());
    }

    //if you want to test that without SpringContext you should add 2 more parameters to the isCardPlayable().
    // Set<Card> playedCards and DealerService dealerService
//    @Test
//    public void isCardPlayable_method_working_properly()
//    {
//        BelotPlayer player = new BelotPlayer();
//        Set<Card> playedCards = new LinkedHashSet<>();
//        playedCards.add(card1);
//        playedCards.add(card3);
//        playedCards.add(new Card(Suit.CLUBS, CardValue.NINE));
//
//        Set<Card> myCards = new HashSet<>();
//        myCards.add(card2);
//        myCards.add(card4);
//        player.setCards(myCards);
//
//        assertTrue(playService.isCardPlayable(card2, AnnounceType.CLUBS, player, playedCards,dealerServiceImpl));
//        assertTrue(playService.isCardPlayable(card2, AnnounceType.DIAMONDS, player, playedCards,dealerServiceImpl));
//        assertFalse(playService.isCardPlayable(card4, AnnounceType.CLUBS, player, playedCards,dealerServiceImpl));
//        assertFalse(playService.isCardPlayable(card4, AnnounceType.DIAMONDS, player, playedCards,dealerServiceImpl));
//    }
//
//    @Test
//    public void isCardPlayable_method_working_properly2()
//    {
//        BelotPlayer player = new BelotPlayer();
//        Set<Card> playedCards = new LinkedHashSet<>();
//        playedCards.add(card1);
//        playedCards.add(card2);
//
//        Set<Card> myCards = new HashSet<>();
//        myCards.add(card3);
//        myCards.add(card4);
//        Card card5 = new Card(Suit.CLUBS, CardValue.NINE);
//        myCards.add(card5);
//        player.setCards(myCards);
//
//        assertFalse(playService.isCardPlayable(card3, AnnounceType.CLUBS, player, playedCards,dealerServiceImpl));
//        assertFalse(playService.isCardPlayable(card3, AnnounceType.ALL_TRUMPS, player, playedCards,dealerServiceImpl));
//        assertFalse(playService.isCardPlayable(card3, AnnounceType.NO_TRUMPS, player, playedCards,dealerServiceImpl));
//        assertFalse(playService.isCardPlayable(card4, AnnounceType.CLUBS, player, playedCards,dealerServiceImpl));
//        assertFalse(playService.isCardPlayable(card4, AnnounceType.ALL_TRUMPS, player, playedCards,dealerServiceImpl));
//        assertFalse(playService.isCardPlayable(card4, AnnounceType.NO_TRUMPS, player, playedCards,dealerServiceImpl));
//        assertTrue(playService.isCardPlayable(card5, AnnounceType.CLUBS, player, playedCards,dealerServiceImpl));
//        assertTrue(playService.isCardPlayable(card5, AnnounceType.ALL_TRUMPS, player, playedCards,dealerServiceImpl));
//        assertTrue(playService.isCardPlayable(card5, AnnounceType.NO_TRUMPS, player, playedCards,dealerServiceImpl));
//    }
}
