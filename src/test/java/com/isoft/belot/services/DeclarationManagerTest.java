package com.isoft.belot.services;

import com.isoft.belot.domain.entities.BelotPlayer;
import com.isoft.belot.domain.entities.Card;
import com.isoft.belot.domain.entities.enums.*;
import com.isoft.belot.game.InMemoryBelots;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * @author Aleksandar Todorov
 */
public class DeclarationManagerTest
{
    private DeclarationManager declarationManager = new DeclarationManager();
    private Card card1;
    private Card card2;
    private Card card3;
    private Card card4;
    private Set<Card> cards;
    private BelotPlayer belotPlayer;

    @Before
    public void setUp() throws Exception
    {
        cards = new LinkedHashSet<>();
        card1 = new Card(Suit.CLUBS, CardValue.TEN);
        card2 = new Card(Suit.CLUBS, CardValue.JACK);
        card3 = new Card(Suit.DIAMONDS, CardValue.TEN);
        card4 = new Card(Suit.DIAMONDS, CardValue.ACE);
        InMemoryBelots.teamsDeclarations.put("1", new HashMap<>());
        Map<Team, List<Declaration>> teamsDeclarationsMap = new EnumMap<>(Team.class);
        teamsDeclarationsMap.put(Team.TEAM_ONE_FIRST, new ArrayList<>());
        teamsDeclarationsMap.put(Team.TEAM_ONE_SECOND, new ArrayList<>());
        teamsDeclarationsMap.put(Team.TEAM_TWO_FIRST, new ArrayList<>());
        teamsDeclarationsMap.put(Team.TEAM_TWO_SECOND, new ArrayList<>());
        InMemoryBelots.teamsDeclarations.put("1", teamsDeclarationsMap);
        belotPlayer = new BelotPlayer();
        belotPlayer.setTeam(Team.TEAM_ONE_FIRST);
    }

    @Test
    public void findSquareDeclarations()
    {
        cards.add(card1);
        cards.add(card2);
        cards.add(new Card(Suit.HEARTS, CardValue.TEN));
        cards.add(new Card(Suit.SPADES, CardValue.TEN));
        cards.add(new Card(Suit.DIAMONDS, CardValue.NINE));
        cards.add(card3);
        cards.add(new Card(Suit.DIAMONDS, CardValue.JACK));
        cards.add(card4);

        assertEquals(1, declarationManager.findSquareDeclarations(cards).size());
        assertEquals(Declaration.SQUARE, declarationManager.findSquareDeclarations(cards).get(0));
        assertEquals(CardValue.TEN, declarationManager.findSquareDeclarations(cards).get(0).getCardValue());
    }

    @Test
    public void findChainDeclarations()
    {
        cards.add(card1);
        cards.add(card2);
        cards.add(new Card(Suit.CLUBS, CardValue.QUEEN));
        cards.add(new Card(Suit.CLUBS, CardValue.KING));
        cards.add(new Card(Suit.DIAMONDS, CardValue.NINE));
        cards.add(card3);
        cards.add(new Card(Suit.DIAMONDS, CardValue.JACK));
        cards.add(card4);

        assertEquals(2, declarationManager.findChainDeclarations(cards).size());
        assertEquals(Declaration.CHAIN_OF_4, declarationManager.findChainDeclarations(cards).get(0));
        assertEquals(CardValue.KING, declarationManager.findChainDeclarations(cards).get(0).getCardValue());
        assertEquals(Declaration.CHAIN_OF_3, declarationManager.findChainDeclarations(cards).get(1));
        assertEquals(CardValue.JACK, declarationManager.findChainDeclarations(cards).get(1).getCardValue());
    }

    @Test
    public void checkForBelotDeclaration_1()
    {
        cards.add(card1);
        cards.add(card2);
        Card card5 = new Card(Suit.CLUBS, CardValue.QUEEN);
        cards.add(card5);
        Card card6 = new Card(Suit.CLUBS, CardValue.KING);
        cards.add(card6);
        belotPlayer.setCards(cards);
        declarationManager.checkForBelotDeclaration("1", belotPlayer, card5, AnnounceType.CLUBS);
        declarationManager.checkForBelotDeclaration("1", belotPlayer, card5, AnnounceType.DIAMONDS);
        assertEquals(1, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).size());
        assertEquals(20, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).get(0).getPoints());
        assertEquals(Declaration.BELOT, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).get(0));
    }

    @Test
    public void checkForBelotDeclaration_2()
    {
        cards.add(card1);
        cards.add(card2);
        Card card6 = new Card(Suit.CLUBS, CardValue.KING);
        cards.add(card6);
        belotPlayer.setCards(cards);

        declarationManager.checkForBelotDeclaration("1", belotPlayer, card6, AnnounceType.CLUBS);
        assertEquals(0, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).size());
    }

    //if you want to test this you must inject dealerService in DeclarationManager
//    @Test
//    public void checkForDeclarations()
//    {
//        cards.add(card1);
//        cards.add(card2);
//        cards.add(new Card(Suit.CLUBS, CardValue.QUEEN));
//        cards.add(new Card(Suit.CLUBS, CardValue.KING));
//        cards.add(new Card(Suit.DIAMONDS, CardValue.KING));
//        cards.add(new Card(Suit.HEARTS, CardValue.KING));
//        cards.add(new Card(Suit.SPADES, CardValue.KING));
//        cards.add(new Card(Suit.SPADES, CardValue.ACE));
//        belotPlayer.setCards(cards);
//        declarationManager.checkForDeclarations("1", belotPlayer, AnnounceType.SPADES);
//
//        assertEquals(2, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).size());
//
//        assertEquals(100, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).get(0).getPoints());
//        assertEquals(CardValue.KING, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).get(0).getCardValue());
//
//        assertEquals(20, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).get(1).getPoints());
//        assertEquals(CardValue.QUEEN, InMemoryBelots.teamsDeclarations.get("1").get(Team.TEAM_ONE_FIRST).get(1).getCardValue());
//    }
}